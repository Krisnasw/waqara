import { createStackNavigator } from "react-navigation";
import SplashScreen from "./components/SplashScreen";
import HomeScreen from "./components/Home";
import LoginScreen from "./components/Login";
import OtpScreen from "./components/OtpScreen";
import RegisterScreen from "./components/Register";
import UmrohFormScreen from "./components/umroh/UmrohForm";
import UmrohListScreen from "./components/umroh/UmrohList";
import UmrohWishlistScreen from "./components/umroh/UmrohWishlist";
import UmrohDetailScreen from "./components/umroh/UmrohDetail";
import UmrohSummaryScreen from "./components/umroh/UmrohSummary";
import UmrohPaymentScreen from "./components/umroh/UmrohPayment";
import UmrohPayByWaqaraScreen from "./components/umroh/UmrohPayByWaqara";
import UmrohPayByTrxScreen from "./components/umroh/UmrohPayByTrx";
import UmrohPayByCcScreen from "./components/umroh/UmrohPayByCC";
import UmrohPembiayaanScreen from "./components/umroh/UmrohPembiayaan";
import UmrohPembiayaanDetailScreen from "./components/umroh/UmrohPembiayaanDetail";
import UmrohSimulasiScreen from "./components/umroh/UmrohSimulasi";
import UmrohSimulasiDetailScreen from "./components/umroh/UmrohSimulasiDetail";
import TabunganIndexScreen from "./components/tabungan/TabunganIndex";
import TabunganAddScreen from "./components/tabungan/TabunganAdd";
import TabunganWdScreen from "./components/tabungan/TabunganWd";
import TabunganWdProccessScreen from "./components/tabungan/TabunganWdProccess";
import TabunganCreateScreen from "./components/tabungan/TabunganCreate";
import TabunganNewScreen from "./components/tabungan/TabunganNew";
import TabunganNewProccessScreen from "./components/tabungan/TabunganNewProccess";
import TabunganActivationScreen from "./components/tabungan/TabunganActivation";
import TabunganPlanScreen from "./components/tabungan/TabunganPlan";
import TabunganPlanProccessScreen from "./components/tabungan/TabunganPlanProccess";
import NewsIndexScreen from "./components/berita/NewsIndex";
import NewsDetailScreen from "./components/berita/NewsDetail";
import InvestasiIndexScreen from "./components/investasi/InvestasiIndex";
import React from 'react';
import {
    View,
    Share
} from 'react-native';
import {
    Icon
} from 'native-base';

export default Router = createStackNavigator(
    {
        Splash: {
            screen: SplashScreen,
            navigationOptions: {
                header: null
            }
        },
        Otp: {
            screen: OtpScreen,
            navigationOptions: {
                header: null
            }
        },
        Login: {
            screen: LoginScreen,
            navigationOptions: {
                header: null
            }
        },
        Register: {
            screen: RegisterScreen,
            navigationOptions: {
                header: null
            }
        },
        Home: {
            screen: HomeScreen,
            navigationOptions: {
                header: null
                // title: 'Home',
                // headerLeft: null
            }
        },
        UmrohForm: {
            screen: UmrohFormScreen,
            navigationOptions: {
                title: "Umroh",
                headerTintColor: "#41854A",
                headerTitleStyle: {
                    color: "#41854A"
                },
                headerRight: null
            }
        },
        UmrohList: {
            screen: UmrohListScreen,
            navigationOptions: ({ navigation }) => ({
                title: "Umroh",
                headerTintColor: "#41854A",
                headerTitleStyle: {
                    color: "#41854A"
                },
                headerRight: <Icon name="heart" style={{ color: "#41854A", marginRight: 20 }} onPress={() => navigation.navigate('UmrohWishlist')} />
            })
        },
        UmrohWishlist: {
            screen: UmrohWishlistScreen,
            navigationOptions: {
                title: "Wishlist",
                headerTintColor: "#41854A",
                headerTitleStyle: {
                    color: "#41854A"
                }
            }
        },
        UmrohDetail: {
            screen: UmrohDetailScreen,
            navigationOptions: ({ navigation }) => ({
                title: "Umroh",
                headerTintColor: "#41854A",
                headerTitleStyle: {
                    color: "#41854A"
                },
                headerRight: 
                    <View style={{ flexDirection: "row" }}>
                        <Icon name="share" style={{ color: "#41854A", marginRight: 20 }} onPress={() => 
                            Share.share({
                                message: 'Shareeee',
                                url: 'http://eisbetech.com',
                                title: 'Wow, did you see that?'
                            }, {
                                // Android only:
                                dialogTitle: 'Share',
                                // iOS only:
                                excludedActivityTypes: [
                                    'com.apple.UIKit.activity.PostToTwitter'
                                ]
                            })
                        } />
                        <Icon name="heart" style={{ color: "#41854A", marginRight: 20 }} onPress={() => navigation.navigate('UmrohWishlist')} />
                    </View>
            })
        },
        UmrohSummary: {
            screen: UmrohSummaryScreen,
            navigationOptions: {
                title: "Ringkasan Pemesanan",
                headerTintColor: "#41854A",
                headerTitleStyle: {
                    color: "#41854A"
                },
                headerRight: null
            }
        },
        UmrohPayment: {
            screen: UmrohPaymentScreen,
            navigationOptions: {
                title: "Pembayaran",
                headerTintColor: "#41854A",
                headerTitleStyle: {
                    color: "#41854A"
                },
                headerRight: null
            }
        },
        UmrohPayByWaqara: {
            screen: UmrohPayByWaqaraScreen,
            navigationOptions: {
                title: "Tabungan Waqara",
                headerTintColor: "#41854A",
                headerTitleStyle: {
                    color: "#41854A"
                },
                headerRight: null
            }
        },
        UmrohPayByTrx: {
            screen: UmrohPayByTrxScreen,
            navigationOptions: {
                title: "Tabungan Waqara",
                headerTintColor: "#41854A",
                headerTitleStyle: {
                    color: "#41854A"
                },
                headerRight: null
            }
        },
        UmrohPayByCC: {
            screen: UmrohPayByCcScreen,
            navigationOptions: {
                title: "Kartu Kredit Syariah",
                headerTintColor: "#41854A",
                headerTitleStyle: {
                    color: "#41854A"
                },
                headerRight: null
            }
        },
        UmrohPembiayaan: {
            screen: UmrohPembiayaanScreen,
            navigationOptions: {
                title: "Pembiayaan",
                headerTintColor: "#41854A",
                headerTitleStyle: {
                    color: "#41854A"
                },
                headerRight: null
            }
        },
        UmrohPembiayaanDetail: {
            screen: UmrohPembiayaanDetailScreen,
            navigationOptions: {
                title: "BNI Syariah",
                headerTintColor: "#41854A",
                headerTitleStyle: {
                    color: "#41854A"
                },
                headerRight: null
            }
        },
        UmrohSimulasi: {
            screen: UmrohSimulasiScreen,
            navigationOptions: {
                title: "Simulasi Pembiayaan",
                headerTintColor: "#41854A",
                headerTitleStyle: {
                    color: "#41854A"
                },
                headerRight: null
            }
        },
        UmrohSimulasiDetail: {
            screen: UmrohSimulasiDetailScreen,
            navigationOptions: {
                title: "Detail",
                headerTintColor: "#41854A",
                headerTitleStyle: {
                    color: "#41854A"
                },
                headerRight: null
            }
        },
        TabunganIndex: {
            screen: TabunganIndexScreen,
            navigationOptions: {
                title: "Tabungan",
                headerTintColor: "#41854A",
                headerTitleStyle: {
                    color: "#41854A"
                },
                headerRight: null
            }
        },
        TabunganAdd: {
            screen: TabunganAddScreen,
            navigationOptions: {
                title: "Tambah Dana",
                headerTintColor: "#41854A",
                headerTitleStyle: {
                    color: "#41854A"
                },
                headerRight: null
            }
        },
        TabunganWd: {
            screen: TabunganWdScreen,
            navigationOptions: {
                title: "Tarik Dana",
                headerTintColor: "#41854A",
                headerTitleStyle: {
                    color: "#41854A"
                },
                headerRight: null
            }
        },
        TabunganWdProccess: {
            screen: TabunganWdProccessScreen,
            navigationOptions: {
                title: "Tarik Dana",
                headerTitleStyle: {
                    color: "#41854A"
                },
                headerLeft: null,
                headerRight: null
            }
        },
        TabunganCreate: {
            screen: TabunganCreateScreen,
            navigationOptions: {
                title: "Tabungan",
                headerTintColor: "#41854A",
                headerTitleStyle: {
                    color: "#41854A"
                },
                headerRight: null
            }
        },
        TabunganNew: {
            screen: TabunganNewScreen,
            navigationOptions: {
                title: "Buat Tabungan",
                headerTintColor: "#41854A",
                headerTitleStyle: {
                    color: "#41854A"
                },
                headerRight: null
            }
        },
        TabunganNewProccess: {
            screen: TabunganNewProccessScreen,
            navigationOptions: {
                title: "Tabungan",
                headerTintColor: "#41854A",
                headerTitleStyle: {
                    color: "#41854A"
                },
                headerRight: null
            }
        },
        TabunganActivation: {
            screen: TabunganActivationScreen,
            navigationOptions: {
                title: "Aktifkan Tabungan",
                headerTintColor: "#41854A",
                headerTitleStyle: {
                    color: "#41854A"
                },
                headerRight: null
            }
        },
        TabunganPlan: {
            screen: TabunganPlanScreen,
            navigationOptions: {
                title: "Perencanaan Tabungan",
                headerTintColor: "#41854A",
                headerTitleStyle: {
                    color: "#41854A"
                },
                headerRight: null
            }
        },
        TabunganPlanProccess: {
            screen: TabunganPlanProccessScreen,
            navigationOptions: {
                title: "Perencanaan Tabungan",
                headerTintColor: "#41854A",
                headerTitleStyle: {
                    color: "#41854A"
                },
                headerRight: null
            }
        },
        NewsIndex: {
            screen: NewsIndexScreen,
            navigationOptions: {
                title: "Berita",
                headerTintColor: "#41854A",
                headerTitleStyle: {
                    color: "#41854A"
                },
                headerRight: null
            }
        },
        NewsDetail: {
            screen: NewsDetailScreen,
            navigationOptions: {
                title: "Berita Populer",
                headerTintColor: "#41854A",
                headerTitleStyle: {
                    color: "#41854A"
                },
                headerRight: null
            }
        },
        InvestasiIndex: {
            screen: InvestasiIndexScreen,
            navigationOptions: {
                title: "Investasi",
                headerTintColor: "#41854A",
                headerTitleStyle: {
                    color: "#41854A"
                },
                headerRight: null
            }
        }
    },
    {
        initialRouteName: "Splash"
    }
);