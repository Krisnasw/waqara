import React, { Component } from 'react';
import {
    ImageBackground,
    Image,
    Keyboard
} from 'react-native';
import {
    Container,
    Content,
    Form,
    StyleProvider,
    Text,
    Item,
    Input,
    Label,
    Button,
    Icon,
    Footer,
    FooterTab
} from 'native-base';
import getTheme from '../../native-base-theme/components';
import material from '../../native-base-theme/variables/material';

class Login extends Component {
    onLoginPressed() {
        Keyboard.dismiss();
        this.props.navigation.navigate('Home');
    }

    onRegisterPressed() {
        Keyboard.dismiss();
        this.props.navigation.navigate('Register');
    }

    render() {
        const { viewStyles, imgStyles, labelStyle, buttonStyles, buttonTextStyles, dividerStyles, buttonGStyles, backgroundImage } = styles;

        return (
            <StyleProvider style={getTheme(material)}>
                <Container 
                    style={viewStyles}>
                    <ImageBackground
                        style={backgroundImage}
                        resizeMode="stretch"
                        source={require("../images/bg.png")}>
                        <Content
                            keyboardShouldPersistTaps="always"
                            keyboardDismissMode="on-drag">
                            <Image
                                style={imgStyles}
                                source={require("../images/logo_white.png")} />
                            <Form>
                                <Content style={{padding: '5%'}}>
                                    <Item>
                                        <Icon style={{color: '#fff'}} active name='mail' />
                                        <Input 
                                            style={labelStyle}
                                            placeholder='No. Hp atau Email'
                                            placeholderTextColor="#FFFFFF"
                                        />
                                    </Item>
                                    <Item>
                                        <Icon style={{color: '#fff'}} active name='key' />
                                        <Input 
                                            style={labelStyle}
                                            secureTextEntry
                                            placeholder='Password'
                                            placeholderTextColor="#FFFFFF"
                                        />
                                    </Item>

                                    <Button rounded light style={buttonStyles} onPress={() => this.onLoginPressed()}>
                                        <Text style={buttonTextStyles}>Masuk</Text>
                                    </Button>

                                    <Text style={dividerStyles}>
                                    ───────  Atau  ───────
                                    </Text>

                                    <Button iconLeft rounded primary style={buttonStyles}>
                                        <Icon type="FontAwesome" android="facebook" ios="facebook" />
                                        <Text style={{color: "#FFF"}}>Masuk dengan Facebook</Text>
                                    </Button>

                                    <Button iconLeft rounded danger style={buttonGStyles}>
                                        <Icon type="FontAwesome" android="google-plus" ios="google-plus" />
                                        <Text style={{color: "#FFF"}}>Masuk dengan Google</Text>
                                    </Button>
                                </Content>
                            </Form>
                        </Content>
                    </ImageBackground>
                    <Footer>
                        <FooterTab>
                            <Button light full
                                onPress={() => this.onRegisterPressed()}
                            >
                                <Text>Belum Punya Akun ? Daftar</Text>
                            </Button>
                        </FooterTab>
                    </Footer>
                </Container>
            </StyleProvider>
        );
    }
}

const styles = {
    backgroundImage: {
        flex: 1,
        alignSelf: 'auto',
        width: null,
        height: "100%"
    },
    viewStyles: {
        flex: 1,
        backgroundColor: "#41854A"
    },
    imgStyles: {
        justifyContent: "center",
        alignSelf: "center",
        margin: "15%",
        width: 250,
        height: 80
    },
    labelStyle: {
        color: "#FFF"
    },
    buttonStyles: {
        margin: "5%",
        marginLeft: 0,
        marginRight: 0,
        flex: 1,
        justifyContent: "center",
        alignSelf: "center"
    },
    buttonGStyles: {
        marginTop: "1%",
        flex: 1,
        justifyContent: "center",
        alignSelf: "center"
    },
    buttonTextStyles: {
        color: "#41854A"
    },
    dividerStyles: {
        justifyContent: "center",
        alignSelf: "center",
        color: "#FFFFFF"
    }
}

export default Login;
