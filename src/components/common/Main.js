import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    ImageBackground,
    TouchableOpacity,
    Dimensions
} from "react-native";
import {
    Content,
    Header,
    Button,
    Card,
    CardItem,
    Grid,
    Row,
} from 'native-base';

class Main extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { imageStyles, walletStyle, textTabunganStyle, textWalletStyle } = styles;
        const { width, height } = Dimensions.get("window");

        return (
            <Content>
                <Header style={{ backgroundColor: "#FFF", height: 50 }}>
                    <Image
                        style={imageStyles}
                        source={require('../../images/logo_dark.png')}
                    />
                </Header>
                <ImageBackground
                    style={{ flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'center', height: 200 }}
                    source={require('../../images/slide.png')}>
                    {/* <View style={{ justifyContent: 'center', alignItems: 'center', width: '100%' }}>
                        <Text style={textStyles}>Pendaftaran Perjalanan</Text>
                        <Text style={textStyles}>Umroh Tahun 2020 Telah Dibuka!</Text>
                    </View> */}
                </ImageBackground>
                <View style={walletStyle}>
                    <Text style={textTabunganStyle}>Tabungan Waqara</Text>
                    <View style={{ flexDirection: "row" }}>
                        <Text style={textWalletStyle}>Rp</Text>
                        <Text style={{ fontSize: 30, marginLeft: 10, color: "#FFF" }}>0</Text>
                        <Button rounded light style={{ height: 40, position: "absolute", right: 20 }} onPress={() => this.props.navigation.navigate('TabunganCreate')}>
                            <Text style={{ color: "#41854A", margin: 10, fontWeight: "normal", paddingLeft: 10, paddingRight: 10 }}>Buat Sekarang</Text>
                        </Button>
                    </View>
                </View>
                <Grid>
                    <Row style={{ justifyContent: "center" }}>
                        <TouchableOpacity style={{ padding: 3 }} onPress={() => this.props.navigation.navigate('UmrohForm')}>
                            <Card style={{ width: ((width - 10) / 4) - 10, height: ((width - 10) / 4) - 10, shadowOpacity: 0, shadowColor: 'transparent' }}>
                                <CardItem cardBody>
                                    <Image source={require("../../images/icon-01.png")} style={{ height: 75, width: 75, flex: 1 }} />
                                </CardItem>
                            </Card>
                            <Text style={{ alignSelf: "center" }}>Umroh</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ padding: 3 }} onPress={() => this.props.navigation.navigate('TabunganIndex')}>
                            <Card style={{ width: ((width - 10) / 4) - 10, height: ((width - 10) / 4) - 10, shadowOpacity: 0, shadowColor: 'transparent' }}>
                                <CardItem cardBody>
                                    <Image source={require("../../images/icon-02.png")} style={{ height: 75, width: 75, flex: 1 }} />
                                </CardItem>
                            </Card>
                            <Text style={{ alignSelf: "center" }}>Tabungan</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ padding: 3 }} onPress={() => this.props.navigation.navigate('UmrohPembiayaan')}>
                            <Card style={{ width: ((width - 10) / 4) - 10, height: ((width - 10) / 4) - 10, shadowOpacity: 0, shadowColor: 'transparent' }}>
                                <CardItem cardBody>
                                    <Image source={require("../../images/icon-03.png")} style={{ height: 75, width: 75, flex: 1 }} />
                                </CardItem>
                            </Card>
                            <Text style={{ alignSelf: "center" }}>Pembiayaan</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ padding: 3 }} onPress={() => this.props.navigation.navigate('NewsIndex')}>
                            <Card style={{ width: ((width - 10) / 4) - 10, height: ((width - 10) / 4) - 10, shadowOpacity: 0, shadowColor: 'transparent' }}>
                                <CardItem cardBody>
                                    <Image source={require("../../images/icon-04.png")} style={{ height: 75, width: 75, flex: 1 }} />
                                </CardItem>
                            </Card>
                            <Text style={{ alignSelf: "center" }}>Berita</Text>
                        </TouchableOpacity>
                    </Row>
                    <Row style={{ justifyContent: "center" }}>
                        <TouchableOpacity style={{ padding: 3 }}>
                            <Card style={{ width: ((width - 10) / 4) - 10, height: ((width - 10) / 4) - 10, shadowOpacity: 0, shadowColor: 'transparent' }}>
                                <CardItem cardBody>
                                    <Image source={require("../../images/icon-05.png")} style={{ height: 75, width: 75, flex: 1 }} />
                                </CardItem>
                            </Card>
                            <Text style={{ alignSelf: "center" }}>Kiblat</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ padding: 3 }}>
                            <Card style={{ width: ((width - 10) / 4) - 10, height: ((width - 10) / 4) - 10, shadowOpacity: 0, shadowColor: 'transparent' }}>
                                <CardItem cardBody>
                                    <Image source={require("../../images/icon-06.png")} style={{ height: 75, width: 75, flex: 1 }} />
                                </CardItem>
                            </Card>
                            <Text style={{ alignSelf: "center" }}>Waktu Sholat</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ padding: 3 }}>
                            <Card style={{ width: ((width - 10) / 4) - 10, height: ((width - 10) / 4) - 10, shadowOpacity: 0, shadowColor: 'transparent' }}>
                                <CardItem cardBody>
                                    <Image source={require("../../images/icon-07.png")} style={{ height: 75, width: 75, flex: 1 }} />
                                </CardItem>
                            </Card>
                            <Text style={{ alignSelf: "center" }}>Doa Doa</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ padding: 3 }}>
                            <Card style={{ width: ((width - 10) / 4) - 10, height: ((width - 10) / 4) - 10, shadowOpacity: 0, shadowColor: 'transparent' }}>
                                <CardItem cardBody>
                                    <Image source={require("../../images/icon-08.png")} style={{ height: 75, width: 75, flex: 1 }} />
                                </CardItem>
                            </Card>
                            <Text style={{ alignSelf: "center" }}>Al Quran</Text>
                        </TouchableOpacity>
                    </Row>
                </Grid>
            </Content>
        );
    }
}

const styles = {
    imageStyles: {
        width: 150,
        height: 30,
        justifyContent: "center",
        alignSelf: "center"
    },
    container: {
        flex: 1
    },
    textStyles: {
        color: "#FFFFFF",
        fontSize: 20
    },
    walletStyle: {
        height: 100,
        backgroundColor: "#41854A"
    },
    textTabunganStyle: {
        fontSize: 15,
        marginLeft: 20,
        marginTop: 20,
        color: "#FFFFFF"
    },
    textWalletStyle: {
        fontSize: 15,
        marginLeft: 20,
        color: "#FFFFFF"
    },
    bottomBarStyle: {
        backgroundColor: "#FFF"
    }
}

export default Main;
