import React, { Component } from 'react';
import {
    View,
    LayoutAnimation,
    Image
} from 'react-native';
import {
    Content,
    Text,
    Card,
    CardItem,
    Header,
    Thumbnail,
    Button, 
    Icon, 
    Left,
    Right,
    Body
} from 'native-base';

class Notif extends Component {
    componentWillMount() {
        LayoutAnimation.easeInEaseOut();
    }

    render() {
        const { imageStyles } = styles;

        return (
            <Content style={{ flex: 1 }}>
                <Header style={{ backgroundColor: "#FFF", height: 50 }}>
                    <Image
                        style={imageStyles}
                        source={require('../../images/logo_dark.png')}
                    />
                </Header>
                <View style={{ flexDirection: "column", margin: 12 }}>
                    <Card style={{ flex: 0 }}>
                        <CardItem>
                            <Left>
                                <Thumbnail source={require('../../images/icon-01.png')} />
                                <Body>
                                    <Text>Umroh</Text>
                                    <Text note style={{ position: "absolute", right: 0 }}>April 15, 2016</Text>
                                    <Text note style={{ marginTop: 5, textAlign: "justify" }}>
                                        Paket : Umroh Promo Juni 2018, Berhasil Dipesan. Cek Dashboard Untuk Memantau Proses Pemesanan dan Pembayaran
                                    </Text>
                                </Body>
                            </Left>
                        </CardItem>
                    </Card>
                    <Card style={{ flex: 0 }}>
                        <CardItem>
                            <Left>
                                <Thumbnail source={require('../../images/icon-01.png')} />
                                <Body>
                                    <Text>Umroh</Text>
                                    <Text note style={{ position: "absolute", right: 0 }}>April 15, 2016</Text>
                                    <Text note style={{ marginTop: 5, textAlign: "justify" }}>
                                        Paket : Umroh Promo Juni 2018, Berhasil Dipesan. Cek Dashboard Untuk Memantau Proses Pemesanan dan Pembayaran
                                    </Text>
                                </Body>
                            </Left>
                        </CardItem>
                    </Card>
                    <Card style={{ flex: 0 }}>
                        <CardItem>
                            <Left>
                                <Thumbnail source={require('../../images/icon-01.png')} />
                                <Body>
                                    <Text>Umroh</Text>
                                    <Text note style={{ position: "absolute", right: 0 }}>April 15, 2016</Text>
                                    <Text note style={{ marginTop: 5, textAlign: "justify" }}>
                                        Paket : Umroh Promo Juni 2018, Berhasil Dipesan. Cek Dashboard Untuk Memantau Proses Pemesanan dan Pembayaran
                                    </Text>
                                </Body>
                            </Left>
                        </CardItem>
                    </Card>
                </View>
            </Content>
        );
    }
}

const styles = {
    imageStyles: {
        width: 150,
        height: 30,
        justifyContent: "center",
        alignSelf: "center"
    }
}

export default Notif;
