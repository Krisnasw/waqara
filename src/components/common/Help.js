import React, { Component } from 'react';
import {
    View,
    LayoutAnimation,
    Dimensions,
    Image
} from 'react-native';
import {
    Content,
    Text,
    Card,
    CardItem,
    Header,
    Item,
    Input,
    Icon,
    Button,
    Right
} from 'native-base';

class Help extends Component {
    componentWillMount() {
        LayoutAnimation.easeInEaseOut();
    }

    render() {
        const { headerStyle } = styles;

        return (
            <Content>
                <Header style={headerStyle} searchBar>
                    <Item style={{
                        borderRadius: 10,
                        borderWidth: 1,
                        borderColor: '#fff' }}>
                        <Icon name="search" style={{ color: "#41854A" }} />
                        <Input placeholder="Cari Informasi Disini" />
                    </Item>
                </Header>
                <View
                    style={{ flexDirection: "column" }}>
                    <View style={{ margin: 12 }}>
                        <Card>
                            <CardItem style={{ height: 50 }}>
                                <View style={{ flex: 3, flexDirection: "row" }}>
                                    <Image source={require("../../images/icon-01.png")} style={{ height: 50, width: 50 }} />
                                    <Text style={{ alignSelf: "center" }}>Umroh</Text>
                                </View>
                                <Right style={{ flex: 1 }}>
                                    <Icon name="arrow-forward" />
                                </Right>
                            </CardItem>
                        </Card>
                        <Card>
                            <CardItem style={{ height: 50 }}>
                                <View style={{ flex: 3, flexDirection: "row" }}>
                                    <Image source={require("../../images/icon-02.png")} style={{ height: 50, width: 50 }} />
                                    <Text style={{ alignSelf: "center" }}>Tabungan</Text>
                                </View>
                                <Right style={{ flex: 1 }}>
                                    <Icon name="arrow-forward" />
                                </Right>
                            </CardItem>
                        </Card>
                        <Card>
                            <CardItem style={{ height: 50 }}>
                                <View style={{ flex: 3, flexDirection: "row" }}>
                                    <Image source={require("../../images/icon-03.png")} style={{ height: 50, width: 50 }} />
                                    <Text style={{ alignSelf: "center" }}>Pembiayaan</Text>
                                </View>
                                <Right style={{ flex: 1 }}>
                                    <Icon name="arrow-forward" />
                                </Right>
                            </CardItem>
                        </Card>
                        <Card>
                            <CardItem style={{ height: 50 }}>
                                <View style={{ flex: 3, flexDirection: "row" }}>
                                    <Image source={require("../../images/icon-08.png")} style={{ height: 50, width: 50 }} />
                                    <Text style={{ alignSelf: "center" }}>Quran</Text>
                                </View>
                                <Right style={{ flex: 1 }}>
                                    <Icon name="arrow-forward" />
                                </Right>
                            </CardItem>
                        </Card>
                        <Card>
                            <CardItem style={{ height: 50 }}>
                                <View style={{ flex: 3, flexDirection: "row" }}>
                                    <Image source={require("../../images/icon-06.png")} style={{ height: 50, width: 50 }} />
                                    <Text style={{ alignSelf: "center" }}>Waktu Doa</Text>
                                </View>
                                <Right style={{ flex: 1 }}>
                                    <Icon name="arrow-forward" />
                                </Right>
                            </CardItem>
                        </Card>
                        <Card>
                            <CardItem style={{ height: 50 }}>
                                <View style={{ flex: 3, flexDirection: "row" }}>
                                    <Image source={require("../../images/icon-04.png")} style={{ height: 50, width: 50 }} />
                                    <Text style={{ alignSelf: "center" }}>Berita</Text>
                                </View>
                                <Right style={{ flex: 1 }}>
                                    <Icon name="arrow-forward" />
                                </Right>
                            </CardItem>
                        </Card>
                        <Card>
                            <CardItem style={{ height: 50 }} button onPress={() => this.props.navigation.navigate('InvestasiIndex')}>
                                <View style={{ flex: 3, flexDirection: "row" }}>
                                    <Image source={require("../../images/icon-09.png")} style={{ height: 50, width: 50 }} />
                                    <Text style={{ alignSelf: "center" }}>Investasi</Text>
                                </View>
                                <Right style={{ flex: 1 }}>
                                    <Icon name="arrow-forward" />
                                </Right>
                            </CardItem>
                        </Card>
                    </View>
                </View>
            </Content>
        );
    }
}

const styles = {
    headerStyle: {
        backgroundColor: "#41854A"
    }
}

export default Help;
