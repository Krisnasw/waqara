import React, { Component } from 'react';
import {
    View,
    Image,
    LayoutAnimation,
    Divider
} from 'react-native';
import {
    Content,
    Thumbnail,
    Text,
    Card,
    CardItem,
    Header,
    Right,
    Left,
    Body,
    Icon,
    Button
} from 'native-base';
import UserAvatar from 'react-native-user-avatar';

class Profile extends Component {
    componentWillMount() {
        LayoutAnimation.easeInEaseOut();
    }

    render() {
        const { imageStyles, buttonStyles } = styles;

        return (
            <Content>
                <Header style={{ backgroundColor: "#FFF", height: 50 }}>
                    <Image
                        style={imageStyles}
                        source={require('../../images/logo_dark.png')}
                    />
                </Header>
                <View>
                    <Text style={{
                        color: "#41854A", alignSelf: 'flex-end',
                        marginTop: "5%",
                        marginRight: "5%",
                        position: 'absolute'
                    }}>Edit</Text>
                    <View style={{ margin: 20, flexDirection: "row" }}>
                        <UserAvatar size="75" name="Krisna Satria" />
                        <View style={{ flexDirection: "column", marginLeft: 10, marginTop: "1%" }}>
                            <Text>Krisna Satria</Text>
                            <Text>0822 2924 6468</Text>
                            <Text>krisnasw@eisbetech.com</Text>
                        </View>
                    </View>
                </View>
                <View
                    style={{
                        borderBottomColor: 'grey',
                        borderBottomWidth: 1,
                    }}
                />
                <View style={{ flexDirection: "column", margin: 10 }}>
                    <Card style={{ flex: 1 }}>
                        <CardItem>
                            <Left>
                                <Body>
                                    <Text>History Pemesanan</Text>
                                    <Text note style={{ marginTop: 5 }}>
                                        Lihat Rekam Histori Pemesanan Anda
                                    </Text>
                                </Body>
                                <Right>
                                    <Icon name="arrow-forward" />
                                </Right>
                            </Left>
                        </CardItem>
                    </Card>
                    <Card style={{ flex: 1 }}>
                        <CardItem>
                            <Left>
                                <Body>
                                    <Text>Kebijakan Privasi</Text>
                                    <Text note style={{ marginTop: 5 }}>
                                        Lihat Bagaimana Kami Mengatur Data Anda
                                    </Text>
                                </Body>
                                <Right>
                                    <Icon name="arrow-forward" />
                                </Right>
                            </Left>
                        </CardItem>
                    </Card>
                    <Card style={{ flex: 1 }}>
                        <CardItem>
                            <Left>
                                <Body>
                                    <Text>Aturan Penggunaan</Text>
                                    <Text note style={{ marginTop: 5 }}>
                                        Lihat Aturan Penggunaan Aplikasi
                                    </Text>
                                </Body>
                                <Right>
                                    <Icon name="arrow-forward" />
                                </Right>
                            </Left>
                        </CardItem>
                    </Card>
                </View>
                <Button rounded success style={buttonStyles}>
                    <Text>Keluar</Text>
                </Button>
            </Content>
        );
    }
}

const styles = {
    imageStyles: {
        width: 150,
        height: 30,
        justifyContent: "center",
        alignSelf: "center"
    },
    buttonStyles: {
        marginLeft: 10,
        marginRight: 10,
        flex: 1,
        justifyContent: "center",
        alignSelf: "center"
    }
}

export default Profile;
