import React, { Component } from 'react';
import {
    View,
    LayoutAnimation,
    Image,
    ProgressBarAndroid,
    ScrollView,
    Dimensions
} from 'react-native';
import {
    Content,
    Text,
    Header,
    Left,
    Right
} from 'native-base';

class Dashboard extends Component {
    componentWillMount() {
        LayoutAnimation.easeInEaseOut();
    }

    render() {
        const { imageStyles } = styles;
        const { width, height } = Dimensions.get("window");

        return (
            <Content style={{ flex: 1 }}>
                <Header style={{ backgroundColor: "#FFF", height: 50 }}>
                    <Image
                        style={imageStyles}
                        source={require('../../images/logo_dark.png')}
                    />
                </Header>
                <View style={{ flex: 1 }}>
                    <ScrollView style={{ flex: 1, marginBottom: 20 }}>
                        <View style={{
                            marginTop: "5%",
                            backgroundColor: "#FFF",
                            height: 30,
                            paddingTop: 3,
                            borderBottomColor: 'grey',
                            borderBottomWidth: 1,
                            borderTopColor: 'grey',
                            borderTopWidth: 1
                        }}>
                            <Text style={{ marginLeft: "5%", color: "#41854A" }}>Status Pembiayaan</Text>
                        </View>
                        <View style={{
                            height: 100,
                            backgroundColor: "#FFF",
                            borderBottomColor: 'gray',
                            borderBottomWidth: 1
                        }}>
                            <View style={{ flexDirection: "row" }}>
                                <Image
                                    style={{ marginTop: 20, height: 60, resizeMode: "contain" }}
                                    source={require('../../images/logo_bni.png')}
                                />
                                <View style={{ flexDirection: "column", justifyContent: "center", marginTop: "2.5%" }}>
                                    <Text>Flexi Umroh iB Hasanah</Text>
                                    <Text style={{ color: "#69797E" }}>Dalam Proses Pengajuan</Text>
                                </View>
                            </View>
                        </View>

                        <View style={{
                            marginTop: "5%",
                            backgroundColor: "#FFF",
                            height: 30,
                            paddingTop: 3,
                            borderBottomColor: 'grey',
                            borderBottomWidth: 1,
                            borderTopColor: 'grey',
                            borderTopWidth: 1
                        }}>
                            <Text style={{ marginLeft: "5%", color: "#41854A" }}>Tabungan Waqara</Text>
                        </View>
                        <View style={{
                            height: "15%",
                            backgroundColor: "#FFF",
                            borderBottomColor: 'grey',
                            borderBottomWidth: 1
                        }}>
                            <View style={{ flexDirection: "row", marginTop: 10 }}>
                                <Left style={{ marginLeft: "5%" }}>
                                    <Text style={{ color: "#69797E" }}>Umroh Promo Juni 2019</Text>
                                </Left>
                                <Right style={{ marginRight: "5%" }}>
                                    <Text style={{ color: "#69797E" }}>Rp. 30.000.000</Text>
                                </Right>
                            </View>
                            <ProgressBarAndroid
                                style={{ marginLeft: "5%", marginRight: "5%", marginTop: "2%", marginBottom: "2%", transform: [{ scaleX: 1.0 }, { scaleY: 3.5 }] }}
                                styleAttr="Horizontal"
                                indeterminate={false}
                                progress={0.3}
                            />
                            <View style={{ flexDirection: "row", marginBottom: 10 }}>
                                <Left style={{ marginLeft: "5%" }}>
                                    <Text style={{ color: "#41854A" }}>Dana Terkumpul</Text>
                                </Left>
                                <Right style={{ marginRight: "5%" }}>
                                    <Text style={{ color: "#41854A" }}>Rp. 5.000.000</Text>
                                </Right>
                            </View>
                        </View>

                        <View style={{
                            marginTop: "5%",
                            backgroundColor: "#FFF",
                            height: 30,
                            paddingTop: 3,
                            borderBottomColor: 'grey',
                            borderBottomWidth: 1,
                            borderTopColor: 'grey',
                            borderTopWidth: 1
                        }}>
                            <Text style={{ marginLeft: "5%", color: "#41854A" }}>Perjalanan Umroh</Text>
                        </View>
                        <View style={{
                            height: 360,
                            backgroundColor: "#FFF",
                            borderBottomColor: 'grey',
                            borderBottomWidth: 1,
                            flexDirection: "column"
                        }}>
                            <Image
                                style={{ marginLeft: "5%", marginRight: "5%", height: 200, marginTop: 10 }}
                                source={{ uri: 'https://umrohhajiwisata.com/wp-content/uploads/2017/12/Trevel-Umroh-1-780x405.jpg' }}
                            />
                            <Text style={{ fontSize: 15, color: "#41854A", alignSelf: "center", fontStyle: "normal", marginLeft: "5%", marginRight: "5%", marginTop: "2.5%", marginBottom: "2.5%" }}>UMROH PROMO JUNI 2019 ( 9 Hari )</Text>
                            <View style={{
                                borderBottomColor: 'grey',
                                borderBottomWidth: 1,
                                marginBottom: "2.5%"
                            }} />
                            <View style={{
                                flexDirection: "column",
                                marginLeft: "5%",
                                marginRight: "5%" }}>
                                <Text>Berangkat 10 Jun 2019 s.d. 18 Jun 2019 ( 9 Hari )</Text>
                                <Text>Dari: Jakarta | Mendarat: Medinah</Text>
                            </View>
                            <View style={{
                                borderBottomColor: 'grey',
                                borderBottomWidth: 1,
                                marginTop: "4%"
                            }} />
                            <Text style={{ color: "#41854A", alignSelf: "center", fontStyle: "normal", marginLeft: "5%", marginRight: "5%" }}>Isi Data Penumpang</Text>
                        </View>
                    </ScrollView>
                </View>
            </Content>
        );
    }
}

const styles = {
    imageStyles: {
        width: 150,
        height: 30,
        justifyContent: "center",
        alignSelf: "center"
    }
}

export default Dashboard;
