import React, { Component } from 'react';
import {
    View,
    LayoutAnimation,
    Image,
    Dimensions
} from 'react-native';
import {
    Container,
    Content,
    Button,
    Text,
    Footer,
    FooterTab,
    Right,
    Icon
} from 'native-base';
import StarRating from 'react-native-star-rating';

class UmrohDetail extends Component {
    componentWillMount() {
        LayoutAnimation.easeInEaseOut();
    }

    render() {
        const { width, height } = Dimensions.get("window");

        return (
            <Container>
                <Content style={{ backgroundColor: "#FFF" }} showsVerticalScrollIndicator={false}>
                    <Content padder>
                        <Image
                            source={{ uri: 'http://inilampung.com/writable/archives/umroh.jpg' }}
                            style={{
                                height: 200
                            }}
                            resizeMode="cover"
                        />
                        <Text style={{ fontWeight: "bold", fontSize: 15, color: "#41854A", alignSelf: "center", fontStyle: "normal", marginLeft: "5%", marginRight: "5%", marginTop: "2.5%", marginBottom: "2.5%" }}>UMROH PROMO JUNI 2019 ( 9 Hari )</Text>
                        <View style={{
                            borderBottomColor: 'grey',
                            borderBottomWidth: 1,
                            marginBottom: "2.5%"
                        }} />
                        <View style={{
                            flexDirection: "column"
                        }}>
                            <Text>Berangkat 10 Jun 2019 s.d. 18 Jun 2019 ( 9 Hari )</Text>
                            <Text>Dari: Jakarta | Mendarat: Medinah</Text>
                        </View>
                        <View style={{
                            borderBottomColor: 'grey',
                            borderBottomWidth: 1,
                            marginTop: "4%"
                        }} />
                        <View style={{
                            borderBottomColor: 'grey',
                            borderBottomWidth: 1,
                            marginBottom: "2.5%"
                        }} />
                        <View style={{
                            flexDirection: "column"
                        }}>
                            <Text style={{ fontWeight: "bold", fontSize: 15, color: "#41854A" }}>Fasilitas Paket :</Text>
                            <Text>Visa Umroh</Text>
                            <Text>Perlengkapan Umroh</Text>
                            <Text>Tiket Pesawat PP</Text>
                            <Text>Transportasi Bus AC di Arab Saudi</Text>
                            <Text>Air Zamzam</Text>

                            <Text style={{ fontWeight: "bold", fontSize: 15, color: "red", marginTop: 10 }}>Tidak Termasuk :</Text>
                            <Text>Biaya Suntik Meningitis</Text>
                            <Text>Surat Mahram</Text>
                            <Text>Voucher Belanja</Text>
                            <Text>Uang Saku (IDR)</Text>
                            <Text>Uang Saku (SARI)</Text>
                        </View>
                        <View style={{
                            borderBottomColor: 'grey',
                            borderBottomWidth: 1,
                            marginTop: "4%"
                        }} />
                        <View style={{
                            borderBottomColor: 'grey',
                            borderBottomWidth: 1,
                            marginBottom: 10
                        }} />
                        <View style={{
                            flexDirection: "column"
                        }}>
                            <Text style={{ fontWeight: "normal", fontSize: 20, color: "#41854A" }}>Hotel Mekkah :</Text>
                            <StarRating
                                disabled={false}
                                maxStars={5}
                                rating={5}
                                fullStarColor="#fcb040"
                            />
                            <Text>Jabar Omar Hyatt Regency</Text>
                            <Text>Menginap: 4 Malam</Text>
                            <Text>Jarak dari Al - Haram: 0 m</Text>
                            <Text>Fasilitas Hotel</Text>

                            <Text style={{ fontWeight: "normal", fontSize: 20, color: "#41854A", marginTop: 10 }}>Hotel Madinah :</Text>
                            <StarRating
                                disabled={false}
                                maxStars={5}
                                rating={5}
                                fullStarColor="#fcb040"
                            />
                            <Text>Grand Mercure Majlis</Text>
                            <Text>Menginap: 3 Malam</Text>
                            <Text>Jarak dari Al - Haram: 0 m</Text>
                            <Text>Fasilitas Hotel</Text>
                        </View>
                        <View style={{
                            borderBottomColor: 'grey',
                            borderBottomWidth: 1,
                            marginTop: 10
                        }} />
                        <View style={{
                            borderBottomColor: 'grey',
                            borderBottomWidth: 1,
                            marginBottom: 10
                        }} />
                        <View style={{
                            flexDirection: "column"
                        }}>
                            <Text style={{ fontWeight: "normal", fontSize: 20, color: "#41854A" }}>Maskapai</Text>
                            <View style={{ flexDirection: "row", marginTop: 10 }}>
                                <Image
                                    style={{ height: 100, width: 100 }}
                                    source={require('../../images/ic_maskapai.png')}
                                    resizeMode="contain"
                                />
                                <View style={{ flexDirection: "column", padding: 30, flex: 1, flexWrap: 'wrap' }}>
                                    <Text style={{ fontWeight: "bold", color: "#585A5C" }}>CGK - MED</Text>
                                    <Text style={{ fontWeight: "normal", color: "#5d5d60" }}>Scoot Airlines - Economic Class</Text>
                                </View>
                            </View>
                            <View style={{ flexDirection: "row", marginTop: 10 }}>
                                <Text style={{ fontWeight: "normal", color: "#5d5d60", width: 100, textAlign: "center" }}>Dari</Text>
                                <View style={{ flexDirection: "column", flex: 1, flexWrap: 'wrap' }}>
                                    <Text style={{ fontWeight: "normal", color: "#5d5d60" }}>Soekarno - Hatta International Airport Jakarta</Text>
                                </View>
                            </View>
                            <View style={{ flexDirection: "row", marginTop: 10 }}>
                                <Text style={{ fontWeight: "normal", color: "#5d5d60", width: 100, textAlign: "center" }}>Ke</Text>
                                <View style={{ flexDirection: "column", flex: 1, flexWrap: 'wrap' }}>
                                    <Text style={{ fontWeight: "normal", color: "#5d5d60" }}>Prince Mohammad bin Abdulaziz Int Airport, Madinah</Text>
                                </View>
                            </View>
                            <View style={{ flexDirection: "row", marginTop: 10 }}>
                                <Text style={{ fontWeight: "normal", color: "#5d5d60", width: 100, textAlign: "center" }}>Tanggal</Text>
                                <View style={{ flexDirection: "column", flex: 1, flexWrap: 'wrap' }}>
                                    <Text style={{ fontWeight: "normal", color: "#5d5d60" }}>10 Juni 2019</Text>
                                </View>
                            </View>
                            <View style={{ flexDirection: "row", marginTop: 10 }}>
                                <Image
                                    style={{ height: 100, width: 100 }}
                                    source={require('../../images/ic_maskapai.png')}
                                    resizeMode="contain"
                                />
                                <View style={{ flexDirection: "column", padding: 30, flex: 1, flexWrap: 'wrap' }}>
                                    <Text style={{ fontWeight: "bold", color: "#585A5C" }}>MED - CGK</Text>
                                    <Text style={{ fontWeight: "normal", color: "#5d5d60" }}>Scoot Airlines - Economic Class</Text>
                                </View>
                            </View>
                            <View style={{ flexDirection: "row", marginTop: 10 }}>
                                <Text style={{ fontWeight: "normal", color: "#5d5d60", width: 100, textAlign: "center" }}>Dari</Text>
                                <View style={{ flexDirection: "column", flex: 1, flexWrap: 'wrap' }}>
                                    <Text style={{ fontWeight: "normal", color: "#5d5d60" }}>Prince Mohammad bin Abdulaziz Int Airport, Madinah</Text>
                                </View>
                            </View>
                            <View style={{ flexDirection: "row", marginTop: 10 }}>
                                <Text style={{ fontWeight: "normal", color: "#5d5d60", width: 100, textAlign: "center" }}>Ke</Text>
                                <View style={{ flexDirection: "column", flex: 1, flexWrap: 'wrap' }}>
                                    <Text style={{ fontWeight: "normal", color: "#5d5d60" }}>Soekarno - Hatta International Airport Jakarta</Text>
                                </View>
                            </View>
                            <View style={{ flexDirection: "row", marginTop: 10 }}>
                                <Text style={{ fontWeight: "normal", color: "#5d5d60", width: 100, textAlign: "center" }}>Tanggal</Text>
                                <View style={{ flexDirection: "column", flex: 1, flexWrap: 'wrap' }}>
                                    <Text style={{ fontWeight: "normal", color: "#5d5d60" }}>10 Juni 2019</Text>
                                </View>
                            </View>
                        </View>
                    </Content>
                    <View style={{ height: 75, borderTopColor: "#C1C2C2", borderTopWidth: 1, borderBottomColor: "#C1C2C2", borderBottomWidth: 1 }}>
                        <View style={{ flex: 1, flexDirection: "row", marginLeft: 10, marginRight: 10 }}>
                            <Text style={{ alignSelf: "center", color: "#41854A", fontSize: 20 }}>Agenda Perjalanan</Text>
                            <Right>
                                <Icon name="arrow-forward" style={{ color: "#41854A" }} />
                            </Right>
                        </View>
                    </View>
                    <View style={{ height: 75, borderTopColor: "#C1C2C2", borderTopWidth: 1, borderBottomColor: "#C1C2C2", borderBottomWidth: 1 }}>
                        <View style={{ flex: 1, flexDirection: "row", marginLeft: 10, marginRight: 10 }}>
                            <Text style={{ alignSelf: "center", color: "#41854A", fontSize: 20 }}>Persyaratan Dokumen</Text>
                            <Right>
                                <Icon name="arrow-forward" style={{ color: "#41854A" }} />
                            </Right>
                        </View>
                    </View>
                    <View style={{ height: 75, borderTopColor: "#C1C2C2", borderTopWidth: 1, borderBottomColor: "#C1C2C2", borderBottomWidth: 1 }}>
                        <View style={{ flex: 1, flexDirection: "row", marginLeft: 10, marginRight: 10 }}>
                            <Text style={{ alignSelf: "center", color: "#41854A", fontSize: 20 }}>Syarat & Ketentuan</Text>
                            <Right>
                                <Icon name="arrow-forward" style={{ color: "#41854A" }} />
                            </Right>
                        </View>
                    </View>
                    <View style={{ flex: 1, flexWrap: "wrap" }}>
                        <Image
                            style={{ width: 150, height: 150, margin: 10 }}
                            source={require('../../images/logo_panorama.png')}
                            resizeMode="contain"
                        />
                        <Text style={{ marginLeft: 10, marginRight: 10, color: "grey" }}>
                            Panorama Tours adalah Perusahaan Biro Perjalanan Wisata yang mempunyai bidang usaha antara lain :
                                - Umrah Regular, Plus{"\n"}
                                - Haji Khusus{"\n"}
                                - Provider Visa Umrah{"\n"}
                                - Tiketing Domestik, Internasional{"\n"}
                                - Tour Domestik, Internasional{"\n"}
                            {"\n"}
                            Akte Pendirian / SK Kehakiman : Nomor C-21554 HT. 01.01.TH.2004
                            {"\n"}
                            {"\n"}
                            Izin Sebagai Biro Perjalanan Wisata : Nomor 10/JT/1/1.855.4 - 27 Jun 2001
                            {"\n"}
                            {"\n"}
                        </Text>
                        <Button light style={{ alignSelf: "center", marginLeft: 10, marginRight: 10, marginBottom: 20, backgroundColor: "#FFF" }}>
                            <Text style={{ textAlign: "center", color: "#000" }}>Paket Tersedia : 20 Paket</Text>
                        </Button>
                    </View>
                </Content>
                <Footer style={{ borderTopColor: "grey", borderTopWidth: 1, bottom: 0 }}>
                    <FooterTab style={{ backgroundColor: "#FFF" }}>
                        <View style={{ marginLeft: 10 }}>
                            <Text style={{ color: "#757477" }}>Biaya Mulai</Text>
                            <Text style={{ color: "#41854A", fontSize: 20, fontWeight: "bold" }}>Rp 25.000.000</Text>
                        </View>
                        <View style={{ justifyContent: "center", alignItems: "center" }}>
                            <Button rounded style={{ backgroundColor: "#41854A", marginTop: 10, marginBottom: 10, marginRight: 10 }} onPress={ () => this.props.navigation.navigate('UmrohSummary') }>
                                <Text style={{ color: "#FFF" }}>Pesan Paket</Text>
                            </Button>
                        </View>
                    </FooterTab>
                </Footer>
            </Container>
        );
    }
}

export default UmrohDetail;
