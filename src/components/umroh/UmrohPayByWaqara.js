import React, { Component } from 'react';
import {
    View,
    LayoutAnimation,
    Image,
    Picker
} from 'react-native';
import {
    Container,
    Content,
    Text,
    Button,
    Right,
    Label,
    Icon
} from 'native-base';

class UmrohPayByWaqara extends Component {
    componentWillMount() {
        LayoutAnimation.easeInEaseOut();
    }

    render() {
        return (
            <Container>
                <Content>
                    <View style={{ backgroundColor: "#FFF", alignItems: "center", marginBottom: 20 }}>
                        <Image
                            style={{ width: 100, height: 100, alignSelf: "center" }}
                            source={require('../../images/logo_bni.png')}
                            resizeMode="contain"
                        />
                        <Text>Tipe Rekening : ib Baitullah</Text>
                        <Text>No Rekening 123456789</Text>
                        <Text>A/N.Abdullah Wijaya</Text>
                    </View>
                    <View style={{
                        borderBottomColor: 'grey',
                        borderBottomWidth: 1
                    }} />
                    <Text style={{ color: 'red', alignSelf: 'center', padding: 20 }}>Saldo tabungan Anda tidak mencukupi</Text>
                    <View style={{ padding: 5, flexDirection: 'row', flexWrap: 'wrap', marginLeft: 10, marginRight: 10 }}>
                        <Text style={{ color: "#000" }}>Total Pesanan</Text>
                        <Right>
                            <Text style={{ color: "#000", fontWeight: "bold" }}>Rp 25.250.000</Text>
                        </Right>
                    </View>
                    <View style={{ padding: 5, flexDirection: 'row', flexWrap: 'wrap', marginLeft: 10, marginRight: 10 }}>
                        <Text style={{ color: "#000" }}>Saldo tabungan saat ini</Text>
                        <Right>
                            <Text style={{ color: "#41854A", fontWeight: "bold" }}>Rp 1.000.000</Text>
                        </Right>
                    </View>
                    <View style={{ padding: 5, flexDirection: 'row', flexWrap: 'wrap', marginLeft: 10, marginRight: 10 }}>
                        <Text style={{ color: "#000" }}>Jumlah yang perlu ditambah</Text>
                        <Right>
                            <Text style={{ color: "#000", fontWeight: "bold" }}>Rp 24.250.000</Text>
                        </Right>
                    </View>
                    <View style={{
                        marginTop: 10,
                        borderBottomColor: 'grey',
                        borderBottomWidth: 1
                    }} />
                    <View style={{
                        height: 35,
                        backgroundColor: "#F7F8F9"
                    }} />
                    <View style={{ padding: 20 }}>
                        <Label style={{ fontSize: 15, color: "black" }}>Tambah saldo tabungan melalui</Label>
                        <Picker
                            selectedValue="all"
                            style={{ height: 50, width: "100%" }}>
                            <Picker.Item label="ATM / Internet / Mobile Banking" value="all" />
                            <Picker.Item label="ATM" value="atm" />
                            <Picker.Item label="Internet" value="inet" />
                            <Picker.Item label="Mobile Banking" value="mbanking" />
                        </Picker>

                        <Text style={{ color: "#000", fontWeight: "bold", marginBottom: 10 }}>Instruksi</Text>
                        <Text>1. Kunjungi jaringan ATM Bersama / Prima</Text>
                        <Text>2. Pilih menu Transfer</Text>
                        <Text>3. Pilih Bank BNI Syariah atau Masukkan Kode Bank 247</Text>
                        <Text>4. Masukkan No. Tabungan Waqara Umroh Anda</Text>
                        <Text style={{ alignSelf: "center", fontWeight: "bold" }}>123 456 789</Text>
                        <Text>5. Masukkan jumlah saldo yang ingin ditambah</Text>
                        <Text>6. Tunggu notifikasi, Saldo anda akan bertambah dalam waktu 1 x 24 jam.</Text>

                        <Text style={{ marginTop: 20, marginLeft: 10, marginRight: 10, textAlign: 'center' }}>Dengan mengetuk tombol berikut. Anda telah menyetujui Syarat & Ketentuan dan Kebijakan Privasi Waqara.</Text>
                    </View>

                    <Button rounded style={{ marginBottom: 20, flex: 1, backgroundColor: "#41854A", marginLeft: 25, marginRight: 25 }} onPress={ () => this.props.navigation.navigate('Home') } iconLeft>
                        <Icon type="FontAwesome" name="lock" />
                        <Text>Proses Sekarang</Text>
                    </Button>
                </Content>
            </Container>
        );
    }
}

export default UmrohPayByWaqara;
