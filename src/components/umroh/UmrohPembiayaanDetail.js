import React, { Component } from 'react';
import {
    View,
    LayoutAnimation,
    Image
} from 'react-native';
import {
    Container,
    Content,
    Text,
    Button,
    Icon
} from 'native-base';

class UmrohPembiayaanDetail extends Component {
    componentWillMount() {
        LayoutAnimation.easeInEaseOut();
    }

    render() {
        return (
            <Container>
                <Content>
                    <View style={{ backgroundColor: "#FBF8F9", flexDirection: "row", padding: 20, borderBottomColor: "grey", borderBottomWidth: 1 }}>
                        <Image
                            style={{ width: 75, height: 50, marginLeft: 20 }}
                            source={require('../../images/logo_bni.png')}
                            resizeMode="contain"
                        />
                        <View style={{ flex: 3, flexDirection: "row", marginLeft: 10 }}>
                            <Text style={{ alignSelf: "center", fontSize: 16 }}>Flexi Umroh iB Hasanah</Text>
                        </View>
                    </View>
                    <View style={{ padding: 20 }}>
                        <Text style={{ color: "grey", fontSize: 15 }}>
                            Fleksi iB Hasanah Umroh ( Fleksi Umroh ) {'\n'}
                            Pembiayaan konsumtif bagi anggota masyarakat untuk memenuhi kebutuhan pembelian Jasa Paket Perjalanan Ibadah Umroh melalui BNI Syariah yang telah bekerja sama dengan Travel Agent sesuai dengan Prinsip Syariah.
                        </Text>
                        <View  style={{ marginTop: 10 }} />
                        <Text style={{ color: "#000", fontSize: 15 }}>
                        Keunggulan : 
                        </Text>
                        <Text style={{ color: "grey", fontSize: 15 }}>
                            Proses cepat dengan persyaratan yang mudah sesuai dengan prinsip syariah.{"\n"}
                            Dapat membiayai perjalanan ibadah umroh orang tua/mertua, suami/istri, dan anak-anak.{"\n"}
                            Maksimum pembiayaan Rp. 200 Juta.{"\n"}
                            Jangka Waktu pembiayaan sampai dengan 3 tahun atau 5 tahun untuk Nasabah payroll BNI atau BNI Syariah
                        </Text>
                        <View  style={{ marginTop: 10 }} />
                        <Text style={{ color: "#000", fontSize: 15 }}>
                        Akad : 
                        </Text>
                        <Text style={{ color: "grey", fontSize: 15 }}>
                            Ijarah Multijasa
                        </Text>
                    </View>
                </Content>
                <Button full success onPress={()=> this.props.navigation.navigate('UmrohSimulasi')}>
                    <Text>Simulasi</Text>
                </Button>
            </Container>
        );
    }
}

export default UmrohPembiayaanDetail;
