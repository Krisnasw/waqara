import React, { Component } from 'react';
import {
    View,
    LayoutAnimation,
    Image
} from 'react-native';
import {
    Content,
    Text,
    Card,
    Button,
    CheckBox,
    Icon,
    Input,
    Grid,
    Col
} from 'native-base';
import SegmentedControlTab from 'react-native-segmented-control-tab';

class UmrohSummary extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedIndex: 0,
            indexOfDewasa: 1,
            indexOfBayi: 1
        };
    }

    handleIndexChange = (index) => {
        this.setState({
            ...this.state,
            selectedIndex: index,
        });
    }

    componentWillMount() {
        LayoutAnimation.easeInEaseOut();
    }

    render() {
        return (
            <Content style={{ backgroundColor: "#FFF" }}>
                <Content padder>
                    <View style={{
                        flexDirection: "column",
                        flexWrap: "wrap"
                    }}>
                        <Text style={{ fontWeight: "bold", fontSize: 15, color: "#41854A", fontStyle: "normal", marginTop: "2.5%" }}>UMROH PROMO JUNI 2019 ( 9 Hari )</Text>
                        <Text>Berangkat 10 Jun 2019 s.d. 18 Jun 2019 ( 9 Hari )</Text>
                        <Text>Dari: Jakarta | Mendarat: Medinah</Text>
                    </View>
                </Content>
                <View style={{
                    borderBottomColor: 'grey',
                    borderBottomWidth: 1,
                    marginTop: "4%"
                }} />
                <View style={{
                    borderBottomColor: 'grey',
                    borderBottomWidth: 1,
                    marginBottom: 10
                }} />
                <Content style={{
                    flexDirection: "column"
                }} padder>
                    <Text style={{ fontWeight: "normal", fontSize: 20, color: "#41854A" }}>Hotel Mekkah</Text>
                    <Text>Jabar Omar Hyatt Regency</Text>

                    <Text style={{ fontWeight: "normal", fontSize: 20, color: "#41854A", marginTop: 10 }}>Hotel Madinah</Text>
                    <Text>Grand Mercure Majlis</Text>
                </Content>
                <View style={{
                    borderBottomColor: 'grey',
                    borderBottomWidth: 1,
                    marginTop: 10
                }} />
                <Content style={{
                    flexDirection: "column"
                }} padder>
                    <Text style={{ fontWeight: "normal", fontSize: 20, color: "#41854A" }}>Maskapai</Text>
                    <View style={{ flexDirection: "row", marginTop: 10 }}>
                        <Image
                            style={{ height: 100, width: 100 }}
                            source={require('../../images/ic_maskapai.png')}
                            resizeMode="contain"
                        />
                        <View style={{ flexDirection: "column", padding: 30, flex: 1, flexWrap: 'wrap' }}>
                            <Text style={{ fontWeight: "bold", color: "#585A5C" }}>CGK - MED</Text>
                            <Text style={{ fontWeight: "normal", color: "#5d5d60" }}>Scoot Airlines - Economic Class</Text>
                        </View>
                    </View>
                    <View style={{ flexDirection: "row", marginTop: 10 }}>
                        <Image
                            style={{ height: 100, width: 100 }}
                            source={require('../../images/ic_maskapai.png')}
                            resizeMode="contain"
                        />
                        <View style={{ flexDirection: "column", padding: 30, flex: 1, flexWrap: 'wrap' }}>
                            <Text style={{ fontWeight: "bold", color: "#585A5C" }}>MED - CGK</Text>
                            <Text style={{ fontWeight: "normal", color: "#5d5d60" }}>Scoot Airlines - Economic Class</Text>
                        </View>
                    </View>
                </Content>
                <View style={{
                    borderBottomColor: 'grey',
                    borderBottomWidth: 1
                }} />
                <Content style={{ flex: 1, flexWrap: "wrap" }} padder>
                    <Image
                        style={{ width: 150, height: 150 }}
                        source={require('../../images/logo_panorama.png')}
                        resizeMode="contain"
                    />
                    <Text style={{ marginLeft: 10, marginRight: 10, color: "grey" }}>
                        Panorama Tours adalah Perusahaan Biro Perjalanan Wisata yang mempunyai bidang usaha antara lain :
                            - Umrah Regular, Plus{"\n"}
                        - Haji Khusus{"\n"}
                        - Provider Visa Umrah{"\n"}
                        - Tiketing Domestik, Internasional{"\n"}
                        - Tour Domestik, Internasional{"\n"}
                        {"\n"}
                        Akte Pendirian / SK Kehakiman : Nomor C-21554 HT. 01.01.TH.2004
                        {"\n"}
                        {"\n"}
                        Izin Sebagai Biro Perjalanan Wisata : Nomor 10/JT/1/1.855.4 - 27 Jun 2001
                        {"\n"}
                        {"\n"}
                    </Text>
                </Content>
                <View style={{
                    borderBottomColor: 'grey',
                    borderBottomWidth: 1
                }} />
                <Content padder>
                    <Text style={{ color: "#757477" }}>Biaya Mulai</Text>
                    <Text style={{ color: "#41854A", fontSize: 20, fontWeight: "bold" }}>Rp 25.000.000</Text>

                    <View style={{ flexDirection: "row" }}>
                        <View style={{ flex: 2, justifyContent: "center" }}>
                            <Text>Dewasa</Text>
                        </View>
                        <View style={{ flex: 2, flexDirection: "row", right: 0 }}>
                            <Button light>
                                <Icon type="FontAwesome" name='minus' />
                            </Button>
                            <View>
                                <Input keyboardType="numeric" value="1" style={{ backgroundColor: "#F8F8F8", width: 50, textAlign: 'center' }} />
                            </View>
                            <Button success>
                                <Icon type="FontAwesome" name='plus' />
                            </Button>
                        </View>
                    </View>

                    <View style={{ flexDirection: "row", marginTop: 5 }}>
                        <View style={{ flex: 2, justifyContent: "center" }}>
                            <Text>Bayi Dibawah 1 Tahun</Text>
                        </View>
                        <View style={{ flex: 2, flexDirection: "row", right: 0 }}>
                            <Button light>
                                <Icon type="FontAwesome" name='minus' />
                            </Button>
                            <View>
                                <Input keyboardType="numeric" value="1" style={{ backgroundColor: "#F8F8F8", width: 50, textAlign: 'center' }} />
                            </View>
                            <Button success>
                                <Icon type="FontAwesome" name='plus' />
                            </Button>
                        </View>
                    </View>

                    <View style={{ flexDirection: "row" }}>
                        <View style={{ flex: 1, justifyContent: "center" }}>
                            <Text>Asuransi</Text>
                        </View>
                        <View style={{ flex: 2, flexDirection: "row" }}>
                            <Button light style={{ width: 100, justifyContent: "center" }}>
                                <Text style={{ fontSize: 13 }}>Tidak</Text>
                            </Button>
                            <Button success style={{ width: 100, justifyContent: "center" }}>
                                <Text style={{ fontSize: 13 }}>Ya</Text>
                            </Button>
                        </View>
                    </View>

                    <Grid style={{ marginTop: 10, marginBottom: 10 }}>
                        <Col style={{ borderRadius: 10, borderWidth: 1, borderColor: "#41854A", height: 100, margin: 5 }}>
                            <View style={{ alignItems: "center", justifyContent: "center", flex: 1 }}>
                                <Image 
                                    style={{ width: 100, height: 50 }}
                                    source={{ uri: 'https://2.bp.blogspot.com/-fVOskhWAMQs/WCbreU0FwxI/AAAAAAAABd8/Au4kTOJ9HbQefEPDrHt042_Ks8jGlHu3wCLcB/s1600/bni%2Blife.jpg' }}
                                    resizeMode="contain"
                                />
                                <Text>Rp 250.000</Text>
                            </View>
                        </Col>
                        <Col style={{ borderRadius: 10, borderWidth: 1, margin: 5 }}>
                        <View style={{ alignItems: "center", justifyContent: "center", flex: 1 }}>
                                <Image 
                                    style={{ width: 100, height: 50 }}
                                    source={{ uri: 'https://3.bp.blogspot.com/-iyEDVNJO3hY/WDRZ3Aq8gdI/AAAAAAAAA0k/VOmSUmQzH1009knYNAMnv6Aiv-gFm0IWQCLcB/s1600/Asuransi-Pan-Pacific.jpg' }}
                                    resizeMode="contain"
                                />
                                <Text>Rp 250.000</Text>
                            </View>
                        </Col>
                        <Col style={{ borderRadius: 10, borderWidth: 1, margin: 5 }}>
                        <View style={{ alignItems: "center", justifyContent: "center", flex: 1 }}>
                                <Image 
                                    style={{ width: 100, height: 50 }}
                                    source={{ uri: 'https://i1.wp.com/cianjurindonesia.com/wp-content/uploads/2018/01/1.Logo-Chubb-300x200.png?fit=300%2C200' }}
                                    resizeMode="contain"
                                />
                                <Text>Rp 250.000</Text>
                            </View>
                        </Col>
                    </Grid>

                    <View style={{ flexDirection: "row", marginBottom: 10 }}>
                        <View style={{ flex: 1, justifyContent: "center" }}>
                            <Text>Tipe Pembayaran</Text>
                        </View>
                        <View style={{ flex: 2, flexDirection: "row" }}>
                            <Button success style={{ width: 100, justifyContent: "center" }}>
                                <Text style={{ fontSize: 13 }}>Penuh</Text>
                            </Button>
                            <Button light style={{ width: 100, justifyContent: "center" }}>
                                <Text style={{ fontSize: 13 }}>Bertahap</Text>
                            </Button>
                        </View>
                    </View>

                    <Text>*Pembayaran Penuh Pembayaran lunas sebesar harga total.</Text>
                    <Text>*Pembayaran Bertahap Pembayaran booking fee Rp 5.000.000 Khusus Keberangkatan > 90 Hari</Text>

                    <View style={{ flexDirection: "row", flexWrap: "wrap", justifyContent: "center", marginTop: 10 }}>
                        <CheckBox checked={true} style={{ marginRight: 20 }} />
                        <Text>Saya telah membaca dan menyetujui {'\n'}</Text>
                        <Text style={{ color: "#41854A" }}>Syarat dan Ketentuan Travel</Text>
                    </View>

                    <Button rounded success style={{ 
                        height: 40, 
                        borderColor: "#41854A", 
                        borderWidth: 1, 
                        backgroundColor: "#41854A", 
                        flex: 1, 
                        justifyContent: "center",
                        marginTop: 20,
                        marginBottom: 20
                    }}
                        onPress={() => this.props.navigation.navigate('UmrohPayment') }
                    >
                        <Text style={{ color: "#FFF", margin: 10, fontWeight: "normal", paddingLeft: 10, paddingRight: 10 }}>Pesan Paket Umroh</Text>
                    </Button>
                </Content>
            </Content>
        );
    }
}

export default UmrohSummary;
