import React, { Component } from 'react';
import {
    View,
    LayoutAnimation,
    Image,
    Picker
} from 'react-native';
import {
    Container,
    Content,
    Text,
    Button,
    Form,
    Right,
    Label,
    Item
} from 'native-base';

class UmrohSimulasi extends Component {
    componentWillMount() {
        LayoutAnimation.easeInEaseOut();
    }

    render() {
        return (
            <Container>
                <Content style={{ backgroundColor: "#FBF8F9" }}>
                    <View style={{ backgroundColor: "#FBF8F9", flexDirection: "row", padding: 20, borderBottomColor: "grey", borderBottomWidth: 1 }}>
                        <Image
                            style={{ width: 75, height: 50, marginLeft: 20 }}
                            source={require('../../images/logo_bni.png')}
                            resizeMode="contain"
                        />
                        <View style={{ flex: 3, flexDirection: "row", marginLeft: 10 }}>
                            <Text style={{ alignSelf: "center", fontSize: 16 }}>Flexi Umroh iB Hasanah</Text>
                        </View>
                    </View>
                    <Content style={{ backgroundColor: "#FFF" }}>
                        <Form style={{ paddingLeft: 20, paddingRight: 20, paddingTop: 5 }}>
                            <Item stackedLabel style={{ marginLeft: -1 }}>
                                <Label>Jenis Pembiayaan</Label>
                                <Picker
                                    selectedValue="all"
                                    style={{ height: 50, width: "100%" }}>
                                    <Picker.Item label="Flexi Umroh iB Hasanah" value="all" />
                                </Picker>
                            </Item>
                            <Item stackedLabel style={{ marginLeft: -1 }}>
                                <Label>Pekerjaan / Usaha</Label>
                                <Picker
                                    selectedValue="all"
                                    style={{ height: 50, width: "100%" }}>
                                    <Picker.Item label="Pilih Pekerjaan" value="all" />
                                </Picker>
                            </Item>
                            <Item stackedLabel style={{ marginLeft: -1 }}>
                                <Label>Jangka Waktu / Termin</Label>
                                <Picker
                                    selectedValue="all"
                                    style={{ height: 50, width: "100%" }}>
                                    <Picker.Item label="Pilih Jangka Waktu" value="all" />
                                </Picker>
                            </Item>
                            <Item stackedLabel style={{ marginLeft: -1 }}>
                                <Label>Nilai Objek</Label>
                                <Picker
                                    selectedValue="all"
                                    style={{ height: 50, width: "100%" }}>
                                    <Picker.Item label="Rp 0" value="all" />
                                </Picker>
                            </Item>
                        </Form>
                        <View style={{ flexDirection: "row", justifyContent: "center", padding: 20 }}>
                            <Button rounded success style={{ width: 150, justifyContent: "center", backgroundColor: "#41854A" }}>
                                <Text>Kalkulasi</Text>
                            </Button>
                            <View style={{ flex: 1 }}  />
                            <Button rounded light style={{ width: 150, justifyContent: "center" }}>
                                <Text>Reset</Text>
                            </Button>
                        </View>
                        <View style={{
                            marginTop: 10,
                            borderBottomColor: 'grey',
                            borderBottomWidth: 1
                        }} />
                        <View style={{ padding: 20, backgroundColor: '#F9F9F9' }}>
                            <Text style={{ textAlign: 'center' }}>Simulasi Pengajuan Pembiayaan Fleksi Umroh iB Hasanah dengan Termin 48 Bulan</Text>
                        </View>
                        <View style={{ padding: 20, backgroundColor: '#FFF', flexDirection: "column", borderBottomColor: 'grey', borderBottomWidth: 1 }}>
                            <View style={{ flexDirection: "row", padding: 5 }}>
                                <Text>Harga Umroh</Text>
                                <Right>
                                    <Text>IDR 30.000.000</Text>
                                </Right>
                            </View>
                            <View style={{ flexDirection: "row", padding: 5 }}>
                                <Text>Uang Muka</Text>
                                <Right>
                                    <Text>IDR 30.000.000</Text>
                                </Right>
                            </View>
                            <View style={{ flexDirection: "row", padding: 5 }}>
                                <Text>Nilai Pembiayaan Bank</Text>
                                <Right>
                                    <Text>IDR 30.000.000</Text>
                                </Right>
                            </View>
                            <View style={{ flexDirection: "row", padding: 5 }}>
                                <Text>Total Pembiayaan</Text>
                                <Right>
                                    <Text>IDR 30.000.000</Text>
                                </Right>
                            </View>
                            <View style={{ flexDirection: "row", padding: 5 }}>
                                <Text>Angsuran / Bulan</Text>
                                <Right>
                                    <Text>IDR 30.000.000</Text>
                                </Right>
                            </View>
                            <View style={{ flexDirection: "row", padding: 5 }}>
                                <Text>Minimal Gaji {"\n"}Pendapatan / Bulan</Text>
                                <Right>
                                    <Text>IDR 30.000.000</Text>
                                </Right>
                            </View>
                        </View>

                        <Button rounded style={{ marginTop: 20, marginBottom: 20, flex: 1, backgroundColor: "#41854A", marginLeft: 25, marginRight: 25 }} onPress={ () => this.props.navigation.navigate('UmrohSimulasiDetail') }>
                            <Text>Ajukan Pembiayaan</Text>
                        </Button>
                    </Content>
                </Content>
            </Container>
        );
    }
}

export default UmrohSimulasi;
