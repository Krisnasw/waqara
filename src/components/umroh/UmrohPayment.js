import React, { Component } from 'react';
import {
    View,
    LayoutAnimation,
    Image
} from 'react-native';
import {
    Container,
    Content,
    Card,
    CardItem,
    Text,
    Right,
    Icon
} from 'native-base';

class UmrohPayment extends Component {
    componentWillMount() {
        LayoutAnimation.easeInEaseOut();
    }

    render() {
        return (
            <Container>
                <Content style={{ backgroundColor: "#F8F9F7" }}>
                    <Content style={{ height: 200, backgroundColor: "#FFF", padding: 20, borderBottomColor: 'grey', borderBottomWidth: 1 }}>
                        <View style={{ flexDirection: 'row', flexWrap: 'wrap', flex: 1 }}>
                            <Text style={{ color: "#41854A", fontWeight: 'bold', }}>UMROH PROMO JUNI 2019</Text>
                            <Right>
                                <Text style={{ color: "#C1C2C2", fontWeight: "bold" }}>DETAILS</Text>
                            </Right>
                        </View>
                        <View style={{ padding: 5, flexDirection: 'row', flexWrap: 'wrap', flex: 1 }}>
                            <View style={{ flexDirection: "column"}}>
                                <Text style={{ color: "#C1C2C2", fontSize: 10 }}>Dewasa</Text>
                                <Text style={{ color: "#C1C2C2" }}>1 X Rp 25.000.000</Text>
                            </View>
                            <Right>
                                <Text style={{ color: "#000" }}>Rp 25.000.000</Text>
                            </Right>
                        </View>
                        <View style={{ padding: 5, flexDirection: 'row', flexWrap: 'wrap', flex: 1 }}>
                            <View style={{ flexDirection: "column"}}>
                                <Text style={{ color: "#C1C2C2", fontSize: 10 }}>Asuransi Perjalanan : Diamond</Text>
                                <Text style={{ color: "#C1C2C2" }}>1 X Rp 250.000</Text>
                            </View>
                            <Right>
                                <Text style={{ color: "#000" }}>Rp 250.000</Text>
                            </Right>
                        </View>
                        <View style={{ padding: 5, flexDirection: 'row', flexWrap: 'wrap', flex: 1 }}>
                            <Text style={{ color: "#000" }}>Total Pesanan</Text>
                            <Right>
                                <Text style={{ color: "#000", fontWeight: "bold" }}>Rp 25.250.000</Text>
                            </Right>
                        </View>
                    </Content>

                    <Text style={{ color: "#41854A", padding: 20 }}>Metode Pembayaran</Text>

                    <Card style={{ marginLeft: 10, marginRight: 10, height: 100 }}>
                        <CardItem style={{ height: 50 }} button onPress={ () => this.props.navigation.navigate('UmrohPayByWaqara') }>
                            <View style={{ flex: 3, flexDirection: "row" }}>
                                <Text style={{ alignSelf: "center", fontSize: 10 }}>Tabungan Waqara Umroh</Text>
                            </View>
                            <Right style={{ flex: 1 }}>
                                <Icon name="arrow-forward" />
                            </Right>
                        </CardItem>
                        <View style={{ borderBottomColor: "grey", borderBottomWidth: 1 }} />
                        <View style={{ flexDirection: "row" }}>
                            <Image
                                style={{ width: 50, height: 50, marginLeft: 20 }}
                                source={require('../../images/logo_bni.png')}
                                resizeMode="contain"
                            />
                            <Text style={{ padding: 5, alignSelf: "center" }}>*******89</Text>
                        </View>
                    </Card>

                    <Card style={{ marginLeft: 10, marginRight: 10, height: 100 }}>
                        <CardItem style={{ height: 50 }} button onPress={ () => this.props.navigation.navigate('UmrohPayByTrx') }>
                            <View style={{ flex: 3, flexDirection: "row" }}>
                                <Text style={{ alignSelf: "center", fontSize: 10 }}>Transfer antar Bank / Setoran Tunai</Text>
                            </View>
                            <Right style={{ flex: 1 }}>
                                <Icon name="arrow-forward" />
                            </Right>
                        </CardItem>
                        <View style={{ borderBottomColor: "grey", borderBottomWidth: 1 }} />
                        <View style={{ flexDirection: "row" }}>
                            <Image
                                style={{ width: 50, height: 50, marginLeft: 20 }}
                                source={{ uri: 'https://upload.wikimedia.org/wikipedia/id/thumb/5/55/BNI_logo.svg/1280px-BNI_logo.svg.png' }}
                                resizeMode="contain"
                            />
                        </View>
                    </Card>

                    <Card style={{ marginLeft: 10, marginRight: 10, height: 100 }}>
                        <CardItem style={{ height: 50 }} button onPress={ () => this.props.navigation.navigate('UmrohPayByCC') }>
                            <View style={{ flex: 3, flexDirection: "row" }}>
                                <Text style={{ alignSelf: "center", fontSize: 10 }}>Kartu Kredit Syariah</Text>
                            </View>
                            <Right style={{ flex: 1 }}>
                                <Icon name="arrow-forward" />
                            </Right>
                        </CardItem>
                        <View style={{ borderBottomColor: "grey", borderBottomWidth: 1 }} />
                        <View style={{ flexDirection: "row" }}>
                            <Image
                                style={{ width: 100, height: 50, marginLeft: 20 }}
                                source={{ uri: 'https://camo.githubusercontent.com/1d762edbbb72ba93ea97cdea47ef5e2073aff47a/687474703a2f2f73746f726167652e6a302e686e2f6372656469742d636172642d6c6f676f732d322e706e67' }}
                                resizeMode="contain"
                            />
                        </View>
                    </Card>

                    <Card style={{ marginLeft: 10, marginRight: 10, height: 100 }}>
                        <CardItem style={{ height: 50 }} button onPress={ () => this.props.navigation.navigate('UmrohPembiayaan') }>
                            <View style={{ flex: 3, flexDirection: "row" }}>
                                <Text style={{ alignSelf: "center", fontSize: 10 }}>Bantuan Pembiayaan</Text>
                            </View>
                            <Right style={{ flex: 1 }}>
                                <Icon name="arrow-forward" />
                            </Right>
                        </CardItem>
                        <View style={{ borderBottomColor: "grey", borderBottomWidth: 1 }} />
                        <View style={{ flexDirection: "row" }}>
                            <Image
                                style={{ width: 50, height: 50, marginLeft: 20 }}
                                source={require('../../images/logo_bni.png')}
                                resizeMode="contain"
                            />
                            <Text style={{ padding: 5, alignSelf: "center" }}>BNI Fleksi Umroh iB Hasanah</Text>
                        </View>
                    </Card>
                </Content>
            </Container>
        );
    }
}

export default UmrohPayment;
