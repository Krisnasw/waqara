import React, { Component } from 'react';
import {
    View,
    LayoutAnimation,
    Image,
    Dimensions,
    Picker
} from 'react-native'
import {
    Content,
    Text,
    Icon,
    Form,
    Card,
    CardItem,
    Body,
    Input,
    Button,
    Item,
    Label
} from 'native-base';

class UmrohForm extends Component {
    constructor(props) {
        super(props);
        var date = new Date();
        this.state = {
            language: "all",
            paket: "all",
            bulan: date.getMonth()
        };
    }

    componentWillMount() {
        LayoutAnimation.spring();
    }

    render() {
        const { imageStyles } = styles;
        const { width, height } = Dimensions.get("window");

        return (
            <Content padder>
                <Card style={{ height: 375 }}>
                    <CardItem>
                        <Content>
                            <Form style={{ padding: 10 }}>
                                <Item style={{ marginLeft: -1 }} stackedLabel>
                                    <Label style={{ fontSize: 15, color: "black" }}>Kota Asal</Label>
                                    <Picker
                                        selectedValue={this.state.language}
                                        style={{ height: 50, width: "100%" }}
                                        onValueChange={(itemValue, itemIndex) => this.setState({ language: itemValue })}>
                                        <Picker.Item label="Semua Kota" value="all" />
                                        <Picker.Item label="Surabaya" value="surabaya" />
                                        <Picker.Item label="Jakarta" value="jakarta" />
                                        <Picker.Item label="Bandung" value="bandung" />
                                    </Picker>
                                </Item>
                                <Item style={{ marginLeft: -1 }} stackedLabel>
                                    <Label style={{ fontSize: 15, color: "black" }}>Jenis Paket</Label>
                                    <Picker
                                        selectedValue={this.state.paket}
                                        style={{ height: 50, width: "100%" }}
                                        onValueChange={(itemValue, itemIndex) => this.setState({ paket: itemValue })}>
                                        <Picker.Item label="Semua Paket" value="all" />
                                        <Picker.Item label="Ekonomi" value="ekonomi" />
                                        <Picker.Item label="Eksekutif" value="eksekutif" />
                                        <Picker.Item label="Bisnis" value="bisnis" />
                                    </Picker>
                                </Item>
                                <Item style={{ marginLeft: -1 }} stackedLabel>
                                    <Label style={{ fontSize: 15, color: "black" }}>Bulan Keberangkatan</Label>
                                    <Picker
                                        selectedValue={this.state.bulan}
                                        style={{ height: 50, width: "100%" }}
                                        onValueChange={(itemValue, itemIndex) => this.setState({ bulan: itemValue })}>
                                        <Picker.Item label="Semua Bulan" value="all" />
                                        <Picker.Item label="Januari" value="1" />
                                        <Picker.Item label="Februari" value="2" />
                                        <Picker.Item label="Maret" value="3" />
                                        <Picker.Item label="April" value="4" />
                                        <Picker.Item label="Mei" value="5" />
                                        <Picker.Item label="Juni" value="6" />
                                        <Picker.Item label="Juli" value="7" />
                                        <Picker.Item label="Agustus" value="8" />
                                        <Picker.Item label="September" value="9" />
                                        <Picker.Item label="Oktober" value="10" />
                                        <Picker.Item label="November" value="11" />
                                        <Picker.Item label="Desember" value="12" />
                                    </Picker>
                                </Item>
                                <Item style={{ marginLeft: -1 }} stackedLabel>
                                    <Label style={{ fontSize: 15, color: "black" }}>Jumlah Penumpang</Label>
                                    <Input placeholder="1" keyboardType="numeric" maxLength={10} />
                                </Item>
                            </Form>
                        </Content>
                    </CardItem>
                </Card>
                <Button rounded style={{ marginTop: 20, flex: 1, backgroundColor: "#41854A", marginLeft: 25, marginRight: 25 }} onPress={ () => this.props.navigation.navigate('UmrohList') }>
                    <Text>Cari Paket</Text>
                </Button>
            </Content>
        );
    }
}

const styles = {
    imageStyles: {
        width: 150,
        height: 30,
        justifyContent: "center",
        alignSelf: "center"
    }
}

export default UmrohForm;
