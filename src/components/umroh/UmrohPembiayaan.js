import React, { Component } from 'react';
import {
    View,
    LayoutAnimation,
    Image
} from 'react-native';
import {
    Container,
    Content,
    Text,
    Card,
    CardItem,
    Icon,
    Right
} from 'native-base';

class UmrohPembiayaan extends Component {
    componentWillMount() {
        LayoutAnimation.easeInEaseOut();
    }

    render() {
        return (
            <Container>
                <Content style={{ padding: 20 }}>
                    <Text style={{ textAlign: 'center' }}>
                        Waqara menjamin setiap penggunanya dapat berangkat Umroh dengan menyediakan alternatif bantuan pembiayaan
                    </Text>

                    <Card style={{ marginTop: 20 }}>
                        <CardItem button onPress={ () => this.props.navigation.navigate('UmrohPembiayaanDetail') }>
                            <Image
                                style={{ width: 75, height: 50, marginLeft: 20 }}
                                source={require('../../images/logo_bni.png')}
                                resizeMode="contain"
                            />
                            <View style={{ flex: 3, flexDirection: "row", marginLeft: 10 }}>
                                <Text style={{ alignSelf: "center", fontSize: 10 }}>Flexi Umroh iB Hasanah</Text>
                            </View>
                            <Right style={{ flex: 1 }}>
                                <Icon name="arrow-forward" />
                            </Right>
                        </CardItem>
                    </Card>
                </Content>
            </Container>
        );
    }
}

export default UmrohPembiayaan;
