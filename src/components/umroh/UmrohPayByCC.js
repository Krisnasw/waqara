import React, { Component } from 'react';
import {
    View,
    LayoutAnimation,
    Image
} from 'react-native';
import {
    Container,
    Content,
    Text,
    Button,
    Input,
    Icon,
    Item,
    Form,
    Label,
    Right
} from 'native-base';

class UmrohPayByCC extends Component {
    componentWillMount() {
        LayoutAnimation.easeInEaseOut();
    }

    render() {
        return (
            <Container>
                <Content>
                    <Form style={{ paddingLeft: 20, paddingRight: 20, paddingTop: 5 }}>
                        <Item stackedLabel style={{ marginLeft: -1 }}>
                            <Label>Nomor Kartu Kredit</Label>
                            <Input />
                        </Item>
                        <View style={{ flex:2, flexDirection:"row", justifyContent:'space-between' }}>
                            <View style={{ flex:2 }}>
                                <Item stackedLabel style={{ marginLeft: -1 }}>
                                    <Label>Berlaku Hingga</Label>
                                    <Input />
                                </Item>
                            </View>
                            <View style={{ flex:0.1 }}/>
                            <View style={{ flex:2 }}>
                                <Item stackedLabel style={{ marginLeft: -1 }}>
                                    <Label>CVV</Label>
                                    <Input />
                                </Item>
                            </View>
                        </View>
                        <Item stackedLabel style={{ marginLeft: -1 }}>
                            <Label>Nama di Kartu</Label>
                            <Input />
                        </Item>
                    </Form>
                    <View style={{
                        marginTop: 30,
                        borderBottomColor: 'grey',
                        borderBottomWidth: 1
                    }} />
                    <View style={{ padding: 5, flexDirection: 'row', flexWrap: 'wrap', marginLeft: 10, marginRight: 10, marginTop: 20 }}>
                        <Text style={{ color: "#000" }}>Total Pesanan</Text>
                        <Right>
                            <Text style={{ color: "#41854A", fontWeight: "bold" }}>Rp 25.250.000</Text>
                        </Right>
                    </View>
                    <View style={{ padding: 5, flexDirection: 'row', flexWrap: 'wrap', marginLeft: 10, marginRight: 10 }}>
                        <Text style={{ color: "#000" }}>Kode Unik</Text>
                        <Right>
                            <Text style={{ color: "#000", fontWeight: "bold" }}>- Rp 449</Text>
                        </Right>
                    </View>
                    <View style={{
                        marginTop: 10,
                        borderBottomColor: 'grey',
                        borderBottomWidth: 1
                    }} />
                    <View style={{ padding: 5, flexDirection: 'row', flexWrap: 'wrap', marginLeft: 10, marginRight: 10 }}>
                        <Text style={{ color: "#000" }}>Total Pesanan</Text>
                        <Right>
                            <Text style={{ color: "#000", fontWeight: "bold" }}>Rp 25.250.000</Text>
                        </Right>
                    </View>
                    <View style={{
                        marginTop: 10,
                        borderBottomColor: 'grey',
                        borderBottomWidth: 1
                    }} />
                    <View style={{ backgroundColor: "#FBF9F7", padding: 20, height: 200 }}>
                        <Text style={{ color: "#41854A", textAlign: "center", fontSize: 10, marginTop: 5, marginBottom: 5 }}>
                            Dengan mengetuk tombol berikut. Anda telah menyetujui Syarat & Ketentuan dan Kebijakan Privasi Waqara
                        </Text>
                        <Button rounded style={{ marginBottom: 20, flex: 1, backgroundColor: "#41854A", marginLeft: 25, marginRight: 25, marginTop: 20 }} onPress={ () => this.props.navigation.navigate('Home') } iconLeft>
                            <Icon type="FontAwesome" name="lock" />
                            <Text>Proses Sekarang</Text>
                        </Button>
                    </View>
                </Content>
            </Container>
        );
    }
}

export default UmrohPayByCC;
