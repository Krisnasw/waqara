import React, { Component } from 'react';
import {
    View,
    LayoutAnimation,
    Dimensions,
    TouchableOpacity,
    TouchableHighlight,
    Image
} from 'react-native';
import {
    Content,
    Text,
    Form,
    Item,
    Icon,
    Input,
    Col,
    Row,
    Grid,
    Button
} from 'native-base';

class UmrohWishlist extends Component {
    componentWillMount() {
        LayoutAnimation.easeInEaseOut();
    }

    render() {
        const { width, height } = Dimensions.get("window");

        return (
            <Content style={{ backgroundColor: "#FFF" }} padder>
                <View style={{ borderRadius: 10, height: 50, backgroundColor: "#F7F7F7", marginTop: 10 }}>
                    <Form>
                        <Item style={{
                            borderRadius: 10,
                            borderWidth: 1,
                            borderColor: '#fff'
                        }}>
                            <Icon name="search" style={{ color: "#41854A" }} />
                            <Input placeholder="Cari Paket Umroh" />
                        </Item>
                    </Form>
                </View>
                <Text style={{ marginTop: 10, marginBottom: 10 }}>Waqara Umroh</Text>
                <Grid style={{ marginBottom: 10 }}>
                    <Col style={{ padding: 5 }}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('UmrohDetail')}>
                            <Image source={{
                                uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSkXA-kpwkk8CxPXO1aQDXil-YwyyAEPBBmX2FwTt9WUSQareNiKg'
                            }}
                                style={{ height: 100, flex: 1 }}
                                resizeMode="contain" />
                            <Text style={{ textAlign: "justify", marginLeft: 6 }}>Laitatul qadr & I'Tikaf Idul Fitri</Text>
                            <Text style={{ textAlign: "justify", marginLeft: 6, fontSize: 15, color: "#41854A" }}>Rp 25.000.000</Text>
                        </TouchableOpacity>
                    </Col>
                    <Col style={{ padding: 5 }}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('UmrohDetail')}>
                            <Image source={{
                                uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSkXA-kpwkk8CxPXO1aQDXil-YwyyAEPBBmX2FwTt9WUSQareNiKg'
                            }}
                                style={{ height: 100, flex: 1 }}
                                resizeMode="contain" />
                            <Text style={{ textAlign: "justify", marginLeft: 6 }}>Laitatul qadr & I'Tikaf Idul Fitri</Text>
                            <Text style={{ textAlign: "justify", marginLeft: 6, fontSize: 15, color: "#41854A" }}>Rp 25.000.000</Text>
                        </TouchableOpacity>
                    </Col>
                </Grid>
                <Grid style={{ marginBottom: 10 }}>
                    <Col style={{ padding: 5 }}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('UmrohDetail')}>
                            <Image source={{
                                uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSkXA-kpwkk8CxPXO1aQDXil-YwyyAEPBBmX2FwTt9WUSQareNiKg'
                            }}
                                style={{ height: 100, flex: 1 }}
                                resizeMode="contain" />
                            <Text style={{ textAlign: "justify", marginLeft: 6 }}>Laitatul qadr & I'Tikaf Idul Fitri</Text>
                            <Text style={{ textAlign: "justify", marginLeft: 6, fontSize: 15, color: "#41854A" }}>Rp 25.000.000</Text>
                        </TouchableOpacity>
                    </Col>
                    <Col style={{ padding: 5 }}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('UmrohDetail')}>
                            <Image source={{
                                uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSkXA-kpwkk8CxPXO1aQDXil-YwyyAEPBBmX2FwTt9WUSQareNiKg'
                            }}
                                style={{ height: 100, flex: 1 }}
                                resizeMode="contain" />
                            <Text style={{ textAlign: "justify", marginLeft: 6 }}>Laitatul qadr & I'Tikaf Idul Fitri</Text>
                            <Text style={{ textAlign: "justify", marginLeft: 6, fontSize: 15, color: "#41854A" }}>Rp 25.000.000</Text>
                        </TouchableOpacity>
                    </Col>
                </Grid>
            </Content>
        );
    }
}

export default UmrohWishlist;
