import React, { Component } from 'react';
import {
    View,
    LayoutAnimation,
    Dimensions,
    TouchableOpacity,
    Image,
    ScrollView
} from 'react-native';
import {
    Container,
    Content,
    Text,
    Form,
    Item,
    Icon,
    Input,
    Col,
    Grid,
    Button
} from 'native-base';

class UmrohList extends Component {
    constructor(props) {
        super(props);
    }

    componentWillMount() {
        LayoutAnimation.easeInEaseOut();
    }

    render() {
        const { width, height } = Dimensions.get("window");

        return (
            <Container>
                <Content style={{ backgroundColor: "#FFF" }} padder>
                    <View style={{ borderRadius: 10, height: 50, backgroundColor: "#F7F7F7", marginTop: 10 }}>
                        <Form>
                            <Item style={{
                                borderRadius: 10,
                                borderWidth: 1,
                                borderColor: '#fff'
                            }}>
                                <Icon name="search" style={{ color: "#41854A" }} />
                                <Input placeholder="Cari Paket Umroh" />
                            </Item>
                        </Form>
                    </View>
                    <Text style={{ marginTop: 10, marginBottom: 10 }}>Waqara Umroh</Text>
                    <Grid style={{ marginBottom: 10 }}>
                        <Col style={{ padding: 5 }}>
                            <TouchableOpacity onPress={()=> this.props.navigation.navigate('UmrohDetail')}>
                                <Image source={{
                                    uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSkXA-kpwkk8CxPXO1aQDXil-YwyyAEPBBmX2FwTt9WUSQareNiKg'
                                }}
                                    style={{ height: 100, flex: 1 }}
                                    resizeMode="contain" />
                                <Text style={{ textAlign: "justify", marginLeft: 6 }}>Laitatul qadr & I'Tikaf Idul Fitri</Text>
                                <Text style={{ textAlign: "justify", marginLeft: 6, fontSize: 15, color: "#41854A" }}>Rp 25.000.000</Text>
                            </TouchableOpacity>
                        </Col>
                        <Col style={{ padding: 5 }}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('UmrohDetail')}>
                                <Image source={{
                                    uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSkXA-kpwkk8CxPXO1aQDXil-YwyyAEPBBmX2FwTt9WUSQareNiKg'
                                }}
                                    style={{ height: 100, flex: 1 }}
                                    resizeMode="contain" />
                                <Text style={{ textAlign: "justify", marginLeft: 6 }}>Laitatul qadr & I'Tikaf Idul Fitri</Text>
                                <Text style={{ textAlign: "justify", marginLeft: 6, fontSize: 15, color: "#41854A" }}>Rp 25.000.000</Text>
                            </TouchableOpacity>
                        </Col>
                    </Grid>
                    <Grid style={{ marginBottom: 10 }}>
                        <Col style={{ padding: 5 }}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('UmrohDetail')}>
                                <Image source={{
                                    uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSkXA-kpwkk8CxPXO1aQDXil-YwyyAEPBBmX2FwTt9WUSQareNiKg'
                                }}
                                    style={{ height: 100, flex: 1 }}
                                    resizeMode="contain" />
                                <Text style={{ textAlign: "justify", marginLeft: 6 }}>Laitatul qadr & I'Tikaf Idul Fitri</Text>
                                <Text style={{ textAlign: "justify", marginLeft: 6, fontSize: 15, color: "#41854A" }}>Rp 25.000.000</Text>
                            </TouchableOpacity>
                        </Col>
                        <Col style={{ padding: 5 }}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('UmrohDetail')}>
                                <Image source={{
                                    uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSkXA-kpwkk8CxPXO1aQDXil-YwyyAEPBBmX2FwTt9WUSQareNiKg'
                                }}
                                    style={{ height: 100, flex: 1 }}
                                    resizeMode="contain" />
                                <Text style={{ textAlign: "justify", marginLeft: 6 }}>Laitatul qadr & I'Tikaf Idul Fitri</Text>
                                <Text style={{ textAlign: "justify", marginLeft: 6, fontSize: 15, color: "#41854A" }}>Rp 25.000.000</Text>
                            </TouchableOpacity>
                        </Col>
                    </Grid>
                    <Button rounded light style={{ 
                            height: 40, 
                            borderColor: "#41854A", 
                            borderWidth: 1, 
                            backgroundColor: "#FFF", 
                            flex: 1, 
                            justifyContent: "center",
                            marginLeft: 50,
                            marginRight: 50,
                            marginTop: 20
                        }}>
                        <Text style={{ color: "#41854A", margin: 10, fontWeight: "normal", paddingLeft: 10, paddingRight: 10 }}>Lihat Semua</Text>
                    </Button>
                    <View style={{
                        marginTop: 30,
                        borderBottomColor: 'grey',
                        borderBottomWidth: 1,
                        marginBottom: 30
                    }} />
                    <Text style={{ marginTop: 10, marginBottom: 10 }}>Penawaran Saat Ini</Text>
                    <Grid style={{ marginBottom: 10 }}>
                        <Col style={{ padding: 5 }}>
                            <TouchableOpacity onPress={()=> this.props.navigation.navigate('UmrohDetail')}>
                                <Image source={{
                                    uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSkXA-kpwkk8CxPXO1aQDXil-YwyyAEPBBmX2FwTt9WUSQareNiKg'
                                }}
                                    style={{ height: 100, flex: 1 }}
                                    resizeMode="contain" />
                                <Text style={{ textAlign: "justify", marginLeft: 6 }}>Laitatul qadr & I'Tikaf Idul Fitri</Text>
                                <Text style={{ textAlign: "justify", marginLeft: 6, fontSize: 15, color: "#41854A" }}>Rp 25.000.000</Text>
                            </TouchableOpacity>
                        </Col>
                        <Col style={{ padding: 5 }}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('UmrohDetail')}>
                                <Image source={{
                                    uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSkXA-kpwkk8CxPXO1aQDXil-YwyyAEPBBmX2FwTt9WUSQareNiKg'
                                }}
                                    style={{ height: 100, flex: 1 }}
                                    resizeMode="contain" />
                                <Text style={{ textAlign: "justify", marginLeft: 6 }}>Laitatul qadr & I'Tikaf Idul Fitri</Text>
                                <Text style={{ textAlign: "justify", marginLeft: 6, fontSize: 15, color: "#41854A" }}>Rp 25.000.000</Text>
                            </TouchableOpacity>
                        </Col>
                    </Grid>
                    <Grid style={{ marginBottom: 10 }}>
                        <Col style={{ padding: 5 }}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('UmrohDetail')}>
                                <Image source={{
                                    uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSkXA-kpwkk8CxPXO1aQDXil-YwyyAEPBBmX2FwTt9WUSQareNiKg'
                                }}
                                    style={{ height: 100, flex: 1 }}
                                    resizeMode="contain" />
                                <Text style={{ textAlign: "justify", marginLeft: 6 }}>Laitatul qadr & I'Tikaf Idul Fitri</Text>
                                <Text style={{ textAlign: "justify", marginLeft: 6, fontSize: 15, color: "#41854A" }}>Rp 25.000.000</Text>
                            </TouchableOpacity>
                        </Col>
                        <Col style={{ padding: 5 }}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('UmrohDetail')}>
                                <Image source={{
                                    uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSkXA-kpwkk8CxPXO1aQDXil-YwyyAEPBBmX2FwTt9WUSQareNiKg'
                                }}
                                    style={{ height: 100, flex: 1 }}
                                    resizeMode="contain" />
                                <Text style={{ textAlign: "justify", marginLeft: 6 }}>Laitatul qadr & I'Tikaf Idul Fitri</Text>
                                <Text style={{ textAlign: "justify", marginLeft: 6, fontSize: 15, color: "#41854A" }}>Rp 25.000.000</Text>
                            </TouchableOpacity>
                        </Col>
                    </Grid>
                    <Button rounded light style={{ 
                            height: 40, 
                            borderColor: "#41854A", 
                            borderWidth: 1, 
                            backgroundColor: "#FFF", 
                            flex: 1, 
                            justifyContent: "center",
                            marginLeft: 50,
                            marginRight: 50,
                            marginTop: 20
                        }}>
                        <Text style={{ color: "#41854A", margin: 10, fontWeight: "normal", paddingLeft: 10, paddingRight: 10 }}>Lihat Semua</Text>
                    </Button>
                    <View style={{
                        marginTop: 30,
                        borderBottomColor: 'grey',
                        borderBottomWidth: 1,
                        marginBottom: 30
                    }} />
                    <Text>Trending Influencers</Text>
                    <Text style={{ fontSize: 13, marginBottom: 10 }}>Pesan paket umroh bersama para selebritis</Text>
                    <View>
                        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                            <Grid style={{ marginBottom: 10 }}>
                                <Col style={{ padding: 5 }}>
                                    <TouchableOpacity onPress={()=> this.props.navigation.navigate('UmrohDetail')}>
                                        <Image source={{
                                            uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSkXA-kpwkk8CxPXO1aQDXil-YwyyAEPBBmX2FwTt9WUSQareNiKg'
                                        }}
                                            style={{ height: 120, flex: 1 }}
                                            resizeMode="contain" />
                                        <Text style={{ textAlign: "justify", marginLeft: 6 }}>Laitatul qadr & I'Tikaf Idul Fitri</Text>
                                        <Text style={{ textAlign: "justify", marginLeft: 6, fontSize: 15, color: "#41854A" }}>Rp 25.000.000</Text>
                                    </TouchableOpacity>
                                </Col>
                                <Col style={{ padding: 5 }}>
                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('UmrohDetail')}>
                                        <Image source={{
                                            uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSkXA-kpwkk8CxPXO1aQDXil-YwyyAEPBBmX2FwTt9WUSQareNiKg'
                                        }}
                                            style={{ height: 120, flex: 1 }}
                                            resizeMode="contain" />
                                        <Text style={{ textAlign: "justify", marginLeft: 6 }}>Laitatul qadr & I'Tikaf Idul Fitri</Text>
                                        <Text style={{ textAlign: "justify", marginLeft: 6, fontSize: 15, color: "#41854A" }}>Rp 25.000.000</Text>
                                    </TouchableOpacity>
                                </Col>
                            </Grid>
                            <Grid style={{ marginBottom: 10 }}>
                                <Col style={{ padding: 5 }}>
                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('UmrohDetail')}>
                                        <Image source={{
                                            uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSkXA-kpwkk8CxPXO1aQDXil-YwyyAEPBBmX2FwTt9WUSQareNiKg'
                                        }}
                                            style={{ height: 120, flex: 1 }}
                                            resizeMode="contain" />
                                        <Text style={{ textAlign: "justify", marginLeft: 6 }}>Laitatul qadr & I'Tikaf Idul Fitri</Text>
                                        <Text style={{ textAlign: "justify", marginLeft: 6, fontSize: 15, color: "#41854A" }}>Rp 25.000.000</Text>
                                    </TouchableOpacity>
                                </Col>
                                <Col style={{ padding: 5 }}>
                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('UmrohDetail')}>
                                        <Image source={{
                                            uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSkXA-kpwkk8CxPXO1aQDXil-YwyyAEPBBmX2FwTt9WUSQareNiKg'
                                        }}
                                            style={{ height: 120, flex: 1 }}
                                            resizeMode="contain" />
                                        <Text style={{ textAlign: "justify", marginLeft: 6 }}>Laitatul qadr & I'Tikaf Idul Fitri</Text>
                                        <Text style={{ textAlign: "justify", marginLeft: 6, fontSize: 15, color: "#41854A" }}>Rp 25.000.000</Text>
                                    </TouchableOpacity>
                                </Col>
                            </Grid>
                        </ScrollView>
                    </View>
                    <View style={{
                        marginTop: 20,
                        borderBottomColor: 'grey',
                        borderBottomWidth: 1,
                        marginBottom: 30
                    }} />
                    <Text style={{ marginTop: 10, marginBottom: 10 }}>Influencers Deals</Text>
                    <Grid style={{ marginBottom: 10 }}>
                        <Col style={{ padding: 5 }}>
                            <TouchableOpacity onPress={()=> this.props.navigation.navigate('UmrohDetail')}>
                                <Image source={{
                                    uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSkXA-kpwkk8CxPXO1aQDXil-YwyyAEPBBmX2FwTt9WUSQareNiKg'
                                }}
                                    style={{ height: 100, flex: 1 }}
                                    resizeMode="contain" />
                                <Text style={{ textAlign: "justify", marginLeft: 6 }}>Laitatul qadr & I'Tikaf Idul Fitri</Text>
                                <Text style={{ textAlign: "justify", marginLeft: 6, fontSize: 15, color: "#41854A" }}>Rp 25.000.000</Text>
                            </TouchableOpacity>
                        </Col>
                        <Col style={{ padding: 5 }}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('UmrohDetail')}>
                                <Image source={{
                                    uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSkXA-kpwkk8CxPXO1aQDXil-YwyyAEPBBmX2FwTt9WUSQareNiKg'
                                }}
                                    style={{ height: 100, flex: 1 }}
                                    resizeMode="contain" />
                                <Text style={{ textAlign: "justify", marginLeft: 6 }}>Laitatul qadr & I'Tikaf Idul Fitri</Text>
                                <Text style={{ textAlign: "justify", marginLeft: 6, fontSize: 15, color: "#41854A" }}>Rp 25.000.000</Text>
                            </TouchableOpacity>
                        </Col>
                    </Grid>
                    <Grid style={{ marginBottom: 10 }}>
                        <Col style={{ padding: 5 }}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('UmrohDetail')}>
                                <Image source={{
                                    uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSkXA-kpwkk8CxPXO1aQDXil-YwyyAEPBBmX2FwTt9WUSQareNiKg'
                                }}
                                    style={{ height: 100, flex: 1 }}
                                    resizeMode="contain" />
                                <Text style={{ textAlign: "justify", marginLeft: 6 }}>Laitatul qadr & I'Tikaf Idul Fitri</Text>
                                <Text style={{ textAlign: "justify", marginLeft: 6, fontSize: 15, color: "#41854A" }}>Rp 25.000.000</Text>
                            </TouchableOpacity>
                        </Col>
                        <Col style={{ padding: 5 }}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('UmrohDetail')}>
                                <Image source={{
                                    uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSkXA-kpwkk8CxPXO1aQDXil-YwyyAEPBBmX2FwTt9WUSQareNiKg'
                                }}
                                    style={{ height: 100, flex: 1 }}
                                    resizeMode="contain" />
                                <Text style={{ textAlign: "justify", marginLeft: 6 }}>Laitatul qadr & I'Tikaf Idul Fitri</Text>
                                <Text style={{ textAlign: "justify", marginLeft: 6, fontSize: 15, color: "#41854A" }}>Rp 25.000.000</Text>
                            </TouchableOpacity>
                        </Col>
                    </Grid>
                    <Button rounded light style={{ 
                            height: 40, 
                            borderColor: "#41854A", 
                            borderWidth: 1, 
                            backgroundColor: "#FFF", 
                            flex: 1, 
                            justifyContent: "center",
                            marginLeft: 50,
                            marginRight: 50,
                            marginTop: 20,
                            marginBottom: 20
                        }}>
                        <Text style={{ color: "#41854A", margin: 10, fontWeight: "normal", paddingLeft: 10, paddingRight: 10 }}>Lihat Semua</Text>
                    </Button>
                </Content>
            </Container>
        );
    }
}

export default UmrohList;
