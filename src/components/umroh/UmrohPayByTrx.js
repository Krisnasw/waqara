import React, { Component } from 'react';
import {
    View,
    LayoutAnimation,
    Image
} from 'react-native';
import {
    Container,
    Content,
    Text,
    Button,
    Right,
    Icon
} from 'native-base';

class UmrohPayByTrx extends Component {
    componentWillMount() {
        LayoutAnimation.easeInEaseOut();
    }

    render() {
        return (
            <Container>
                <Content>
                    <View style={{ padding: 5, flexDirection: 'row', flexWrap: 'wrap', marginLeft: 10, marginRight: 10, marginTop: 20 }}>
                        <Text style={{ color: "#000" }}>Total Pesanan</Text>
                        <Right>
                            <Text style={{ color: "#41854A", fontWeight: "bold" }}>Rp 25.250.000</Text>
                        </Right>
                    </View>
                    <View style={{ padding: 5, flexDirection: 'row', flexWrap: 'wrap', marginLeft: 10, marginRight: 10 }}>
                        <Text style={{ color: "#000" }}>Kode Unik</Text>
                        <Right>
                            <Text style={{ color: "#000", fontWeight: "bold" }}>- Rp 449</Text>
                        </Right>
                    </View>
                    <View style={{
                        marginTop: 10,
                        borderBottomColor: 'grey',
                        borderBottomWidth: 1
                    }} />
                    <View style={{ padding: 5, flexDirection: 'row', flexWrap: 'wrap', marginLeft: 10, marginRight: 10 }}>
                        <Text style={{ color: "#000" }}>Total Pesanan</Text>
                        <Text style={{ color: "#41854A", fontWeight: "bold", textAlign: 'center', flex: 1 }}>SALIN</Text>
                        <Right>
                            <Text style={{ color: "#000", fontWeight: "bold" }}>Rp 25.250.000</Text>
                        </Right>
                    </View>
                    <Text style={{ color: "#000", marginLeft: 20, marginRight: 20, textAlign: "center", fontSize:  10, marginTop: 10 }}>Penting! Mohon transfer sampai 3 digit terakhir</Text>
                    <View style={{
                        marginTop: 10,
                        borderBottomColor: 'grey',
                        borderBottomWidth: 1
                    }} />
                    <View style={{
                        height: 35,
                        backgroundColor: "#FBF9F7",
                        borderBottomColor: 'grey',
                        borderBottomWidth: 1
                    }} />
                    <View style={{ padding: 20 }}>
                        <View style={{ flexDirection: 'row', flexWrap: 'wrap', flex: 1 }}>
                            <View style={{ flexDirection: "column"}}>
                                <Text style={{ color: "#C1C2C2", fontSize: 15 }}>No Rekening</Text>
                                <Text style={{ color: "#000" }}>123456789</Text>
                            </View>
                            <Right>
                                <Text style={{ color: "#41854A", fontWeight: "bold" }}>SALIN</Text>
                            </Right>
                        </View>
                        <View style={{ marginTop: 20, flexDirection: 'row', flexWrap: 'wrap', flex: 1 }}>
                            <View style={{ flexDirection: "column"}}>
                                <Text style={{ color: "#C1C2C2", fontSize: 15 }}>Nama Pemilik Rekening</Text>
                                <Text style={{ color: "#000" }}>PT Waqara Indonesia</Text>
                            </View>
                            <Right>
                                <Image
                                    style={{ width: 50, height: 50, marginLeft: 20 }}
                                    source={require('../../images/logo_bni.png')}
                                    resizeMode="contain"
                                />
                            </Right>
                        </View>
                    </View>
                    <View style={{
                        borderBottomColor: 'grey',
                        borderBottomWidth: 1
                    }} />
                    <Text style={{ color: "#41854A", textAlign: "center", fontSize: 10, marginTop: 5, marginBottom: 5 }}>
                        Setelah pembayaran Anda dikonfirmasi, kami akan mengirim bukti pembelian dan form pengisian data ke alamat email Anda
                    </Text>
                    <View style={{
                        borderBottomColor: 'grey',
                        borderBottomWidth: 1
                    }} />
                    <View style={{ backgroundColor: "#FBF9F7", padding: 20, height: 200 }}>
                        <Text style={{ color: "#41854A", textAlign: "center", fontSize: 10, marginTop: 5, marginBottom: 5 }}>
                            Dengan melakukan pembayaran. Anda telah menyetujui Syarat & ketentuan dan Kebijakan Privasi Waqara.
                        </Text>
                        <Button rounded style={{ marginBottom: 20, flex: 1, backgroundColor: "#41854A", marginLeft: 25, marginRight: 25, marginTop: 20 }} onPress={ () => this.props.navigation.navigate('Home') } iconLeft>
                            <Icon type="FontAwesome" name="check" />
                            <Text>Saya Sudah Membayar</Text>
                        </Button>
                    </View>
                </Content>
            </Container>
        );
    }
}

export default UmrohPayByTrx;
