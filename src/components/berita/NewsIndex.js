import React, { Component } from 'react';
import {
    View,
    Image,
    LayoutAnimation
} from 'react-native';
import {
    Container,
    Tab,
    Tabs,
    ScrollableTab
} from 'native-base';
import Tab1 from './NewsList';

class NewsIndex extends Component {
    constructor(props) {
        super(props);
    }

    componentWillMount() {
        LayoutAnimation.easeInEaseOut();
    }

    render() {
        return (
            <Container>
                <Tabs renderTabBar={()=> <ScrollableTab />}>
                    <Tab heading="Berita Populer" tabStyle={{backgroundColor: '#FFF'}} textStyle={{color: '#000'}} activeTabStyle={{backgroundColor: '#FFF'}} activeTextStyle={{color: '#41854A', fontWeight: 'normal'}}>
                        <Tab1 navigation={this.props.navigation} />
                    </Tab>
                    <Tab heading="Travel" tabStyle={{backgroundColor: '#FFF'}} textStyle={{color: '#000'}} activeTabStyle={{backgroundColor: '#FFF'}} activeTextStyle={{color: '#41854A', fontWeight: 'normal'}}>
                        <Tab1 navigation={this.props.navigation} />
                    </Tab>
                    <Tab heading="Ekonomi" tabStyle={{backgroundColor: '#FFF'}} textStyle={{color: '#000'}} activeTabStyle={{backgroundColor: '#FFF'}} activeTextStyle={{color: '#41854A', fontWeight: 'normal'}}>
                        <Tab1 navigation={this.props.navigation} />
                    </Tab>
                    <Tab heading="Teknologi" tabStyle={{backgroundColor: '#FFF'}} textStyle={{color: '#000'}} activeTabStyle={{backgroundColor: '#FFF'}} activeTextStyle={{color: '#41854A', fontWeight: 'normal'}}>
                        <Tab1 navigation={this.props.navigation} />
                    </Tab>
                </Tabs>
            </Container>
        );
    }
}

export default NewsIndex;
