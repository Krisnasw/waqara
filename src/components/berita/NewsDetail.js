import React, { Component } from 'react';
import {
    View,
    LayoutAnimation,
    Image,
    Share
} from 'react-native';
import {
    Container,
    Content,
    Text,
    Icon,
    Right
} from 'native-base';

class NewsDetail extends Component {
    componentWillMount() {
        LayoutAnimation.easeInEaseOut();
    }

    render() {
        return (
            <Container>
                <Content>
                    <Image
                        style={{ height: 200, width: null }}
                        resizeMode="stretch"
                        source={{ uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR4ekKn1yeoVC1eSi3jWqYh9aiM24G9Wz8eDhTOFY8HvEsTT5FA' }}
                    />
                    <View style={{ padding: 10 }}>
                        <Text style={{ color: '#41845A', fontSize: 14, fontWeight: "bold" }}>Meningkatnya Popularitas Aplikasi Smartphone Untuk Beribadah</Text>
                        <View style={{ flexDirection: "row" }}>
                            <View style={{ flexDirection: 'column' }}>
                                <Text style={{ color: '#000', fontSize: 12 }}>Detik.com</Text>
                                <Text style={{ color: 'grey', fontSize: 12 }}>21 April 2018</Text>
                            </View>
                            <Right>
                                <Icon type="Ionicons" name="share" onPress={() => 
                                    Share.share({
                                        message: 'Shareeee',
                                        url: 'http://eisbetech.com',
                                        title: 'Wow, did you see that?'
                                    }, {
                                        // Android only:
                                        dialogTitle: 'Share',
                                        // iOS only:
                                        excludedActivityTypes: [
                                            'com.apple.UIKit.activity.PostToTwitter'
                                        ]
                                    })
                                } />
                            </Right>
                        </View>
                    </View>
                    <View style={{
                        borderBottomColor: 'grey',
                        borderBottomWidth: 1
                    }}  />
                    <Text style={{ color: 'black', fontSize: 13, padding: 10 }}>
                        Lorem ipsum dolor sit amet, vim at quot molestiae elaboraret, ad movet pericula reformidans nam, ex his persius indoctum ocurreret. Sea decore accusata ne. Usu ex timeam deleniti, ut mea justo maiestatis, et vis deserunt maluisset. Sanctus maluisset nec te, movet disputando ut vis.
                    </Text>
                </Content>
            </Container>
        );
    }
}

export default NewsDetail;
