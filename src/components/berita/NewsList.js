import React, { Component } from 'react';
import {
    View,
    LayoutAnimation,
    Image,
    ImageBackground,
    TouchableOpacity
} from 'react-native';
import {
    Container,
    Content,
    Text,
    Right
} from 'native-base';

class NewsList extends Component {
    componentWillMount() {
        LayoutAnimation.easeInEaseOut();
    }

    render() {
        return (
            <Container>
                <Content>
                    <ImageBackground
                        style={{ flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'center', height: 200 }}
                        source={{ uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR4ekKn1yeoVC1eSi3jWqYh9aiM24G9Wz8eDhTOFY8HvEsTT5FA' }}>
                        <View style={{position: 'absolute', padding: 5, top: 125, left: 0, right: 0, bottom: 0, backgroundColor: 'rgba(52, 52, 52, 0.8)'}}>
                            <Text style={{ color: 'white' }}>Meningkatnya Popularitas Aplikasi Smartphone Untuk Beribadah</Text>
                            <Text style={{ color: 'white', fontSize: 12 }}>Teknologi</Text>
                        </View>
                    </ImageBackground>

                    <TouchableOpacity onPress={ () => this.props.navigation.navigate('NewsDetail') }>
                        <View style={{ flexDirection: 'row', padding: 10 }}>
                            <View style={{ flexDirection: 'column', width: 200 }}>
                                <Text>Penerbit Buku Islam diberi Insentif oleh Pemerintah</Text>
                                <Text style={{ color: 'grey', fontSize: 12 }}>Travel</Text>
                            </View>
                            <Right>
                                <Image
                                    style={{ width: 150, height: 70, alignSelf: 'center' }}
                                    source={{ uri: 'https://www.mapua.edu.ph/blog/wp-content/uploads/2016/06/DSC_1037-e1465789851788-720x340.jpg' }}
                                    resizeMode="center"
                                />
                            </Right>
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={ () => this.props.navigation.navigate('NewsDetail') }>
                        <View style={{ flexDirection: 'row', padding: 10 }}>
                            <View style={{ flexDirection: 'column', width: 200 }}>
                                <Text>Penerbit Buku Islam diberi Insentif oleh Pemerintah</Text>
                                <Text style={{ color: 'grey', fontSize: 12 }}>Travel</Text>
                            </View>
                            <Right>
                                <Image
                                    style={{ width: 150, height: 70, alignSelf: 'center' }}
                                    source={{ uri: 'https://www.mapua.edu.ph/blog/wp-content/uploads/2016/06/DSC_1037-e1465789851788-720x340.jpg' }}
                                    resizeMode="center"
                                />
                            </Right>
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={ () => this.props.navigation.navigate('NewsDetail') }>
                        <View style={{ flexDirection: 'row', padding: 10 }}>
                            <View style={{ flexDirection: 'column', width: 200 }}>
                                <Text>Penerbit Buku Islam diberi Insentif oleh Pemerintah</Text>
                                <Text style={{ color: 'grey', fontSize: 12 }}>Travel</Text>
                            </View>
                            <Right>
                                <Image
                                    style={{ width: 150, height: 70, alignSelf: 'center' }}
                                    source={{ uri: 'https://www.mapua.edu.ph/blog/wp-content/uploads/2016/06/DSC_1037-e1465789851788-720x340.jpg' }}
                                    resizeMode="center"
                                />
                            </Right>
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={ () => this.props.navigation.navigate('NewsDetail') }>
                        <View style={{ flexDirection: 'row', padding: 10 }}>
                            <View style={{ flexDirection: 'column', width: 200 }}>
                                <Text>Penerbit Buku Islam diberi Insentif oleh Pemerintah</Text>
                                <Text style={{ color: 'grey', fontSize: 12 }}>Travel</Text>
                            </View>
                            <Right>
                                <Image
                                    style={{ width: 150, height: 70, alignSelf: 'center' }}
                                    source={{ uri: 'https://www.mapua.edu.ph/blog/wp-content/uploads/2016/06/DSC_1037-e1465789851788-720x340.jpg' }}
                                    resizeMode="center"
                                />
                            </Right>
                        </View>
                    </TouchableOpacity>
                </Content>
            </Container>
        );
    }
}

export default NewsList;
