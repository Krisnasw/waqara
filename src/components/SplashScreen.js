import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Button,
    Image,
    LayoutAnimation
} from 'react-native';
import { NavigationActions, StackActions } from 'react-navigation';

let timeoutid;

class SplashScreen extends Component {

    static navigationOptions = {
        navbarHidden: true,
        tabBarHidden: true,
    };

    constructor(props) {
        super(props)
        this.state = { navigatenow: false };
    }

    componentDidMount() {
        LayoutAnimation.easeInEaseOut();
        timeoutid = setTimeout(() => {
            this.setState({ navigatenow: true });
        }, 3000);
    }

    componentWillUnmount() {
        clearTimeout(timeoutid);
    }

    componentDidUpdate() {
        const { navigate,goBack } = this.props.navigation;
        if (this.state.navigatenow == true) {
            const navigateAction = StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: "Otp" })],
              });
            
            this.props.navigation.dispatch(navigateAction);
        }
    }

    render() {
        const { viewStyles, imgStyles } = styles;

        return (
            <View style={viewStyles}>
                <Image
                    style={{ width: "100%", height: "15%", resizeMode: "cover" }}
                    source={require("../images/logo_white.png")}
                />
            </View>
        );
    }
}

const styles = {
    viewStyles: {
        backgroundColor: "#41854A",
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    imgStyles: {
        justifyContent: 'center',
        alignSelf: 'center'
    }
}

export default SplashScreen;
