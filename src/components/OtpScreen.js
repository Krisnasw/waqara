import React, { Component } from 'react';
import { View, Text, LayoutAnimation } from 'react-native';
import PinView from 'react-native-pin-view';
import { NavigationActions, StackActions } from 'react-navigation';

class OtpScreen extends Component {
    constructor(props) {
        super(props);
        this.onFailure = this.onFailure.bind(this);
        this.onSuccess = this.onSuccess.bind(this);
    }

    componentWillMount() {
        LayoutAnimation.easeInEaseOut();
    }

    onFailure() {
        alert('FAILURE')
    }

    onSuccess() {
        const navigateAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: "Login" })],
          });
        
        this.props.navigation.dispatch(navigateAction);
    }

    render() {
        const { textStyles } = styles;

        return (
            <View style={{
                flex           : 1,
                backgroundColor: '#f1f1f1',
                justifyContent : 'center'
                }}>
                <Text style={textStyles}>Masukkan Security Code Anda</Text>
                <PinView
                    onSuccess={ this.onSuccess }
                    onFailure={ this.onFailure }
                    password={ [5,5,5,5,5] }
                />
            </View>
        );
    }
}

const styles = {
    textStyles: {
        color: "#000",
        margin: "5%",
        justifyContent: 'center',
        alignSelf: 'center'
    }
}

export default OtpScreen;
