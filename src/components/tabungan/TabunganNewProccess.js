import React, { Component } from 'react';
import {
    View,
    LayoutAnimation,
    Image,
    Dimensions
} from 'react-native';
import {
    Container,
    Content,
    Text,
    Card,
    CardItem,
    Button
} from 'native-base';

class TabunganNewProccess extends Component {
    componentWillMount() {
        LayoutAnimation.easeInEaseOut();
    }

    render() {
        const { width, height } = Dimensions.get("window");

        return (
            <Container>
                <Content style={{ padding: 20 }}>
                    <View style={{ backgroundColor: "#FFF", alignItems: "center", marginBottom: 20 }}>
                        <Image
                            style={{ width: 100, height: 100, alignSelf: "center" }}
                            source={require('../../images/logo_bni.png')}
                            resizeMode="contain"
                        />
                        <Text style={{ color: 'grey', textAlign: 'center' }}>Terima kasih telah mengajukan pembuatan Tabungan iB Baitullah dari BNI Syariah</Text>

                        <Text style={{ color: 'black', textAlign: 'center', fontWeight: "bold", marginTop: 20, fontSize: 15 }}>Untuk mendapatkan nomor rekening tabungan Anda, harap melakukan verifikasi ke Cabang BNI Syariah dengan Membawa KTP Anda dengan menunjukkan kode aktivasi anda sebagai berikut.</Text>
                    </View>

                    <Card style={{ backgroundColor: '#F2F2F2', width: width - 110, alignSelf: 'center' }}>
                        <CardItem style={{ flexDirection: "column", backgroundColor: "#F2F2F2" }}>
                            <Text style={{ color: "#41854A", fontWeight: "bold", fontSize: 30 }}>AGKT1453GJF</Text>
                        </CardItem>
                    </Card>

                    <Text style={{ color: 'grey', textAlign: 'center', marginTop: 20 }}>Kode ini berlaku mulai 1x24 jam sejak pengajuan sampai dengan 14 hari kerja. Setelah melewati periode tersebut kode tidak laku.</Text>

                    <Button success rounded style={{ marginTop: 20, alignSelf: 'center', borderWidth: 1, borderColor: "#FFF", shadowOpacity: 0, backgroundColor: "#41854A" }} onPress={ () => this.props.navigation.navigate('Home') }>
                        <Text style={{ color: '#FFF' }}>Aktifkan Tabungan</Text>
                    </Button>
                </Content>
            </Container>
        );
    }
}

export default TabunganNewProccess;
