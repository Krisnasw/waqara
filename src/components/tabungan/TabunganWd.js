import React, { Component } from 'react';
import {
    View,
    LayoutAnimation,
    Image,
    Picker
} from 'react-native';
import {
    Container,
    Content,
    Text,
    Label,
    Button
} from 'native-base';

class TabunganWd extends Component {
    componentWillMount() {
        LayoutAnimation.easeInEaseOut();
    }

    render() {
        return (
            <Container>
                <Content style={{ padding: 20 }}>
                    <View style={{ backgroundColor: "#FFF", alignItems: "center", marginBottom: 20 }}>
                        <Image
                            style={{ width: 100, height: 100, alignSelf: "center" }}
                            source={require('../../images/logo_bni.png')}
                            resizeMode="contain"
                        />
                        <Text style={{ color: 'grey', textAlign: 'center' }}>Penarikan dana hanya bisa dilakukan melalui cabang BNI Syariah dengan mengisi data dibawah ini</Text>
                    </View>

                    <Label style={{ fontSize: 15, color: "black" }}>Isi Saldo Ke</Label>
                    <Picker
                        selectedValue="all"
                        style={{ height: 50, width: "100%" }}>
                        <Picker.Item label="Tabungan Waqara Umroh" value="all" />
                    </Picker>

                    <Label style={{ fontSize: 15, color: "black" }}>Dari</Label>
                    <Picker
                        selectedValue="all"
                        style={{ height: 50, width: "100%" }}>
                        <Picker.Item label="ATM / Internet / Mobile Banking" value="all" />
                        <Picker.Item label="ATM" value="atm" />
                        <Picker.Item label="Internet" value="inet" />
                        <Picker.Item label="Mobile Banking" value="mbanking" />
                    </Picker>

                    <Button rounded style={{ marginTop: 20, flex: 1, backgroundColor: "#41854A" }} onPress={ () => this.props.navigation.navigate('TabunganWdProccess') }>
                        <Text>Proses Penarikan</Text>
                    </Button>
                </Content>
            </Container>
        );
    }
}

export default TabunganWd;
