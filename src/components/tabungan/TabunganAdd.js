import React, { Component } from 'react';
import {
    View,
    LayoutAnimation,
    Picker
} from 'react-native';
import {
    Container,
    Content,
    Text,
    Label,
    Button
} from 'native-base';

class TabunganAdd extends Component {
    componentWillMount() {
        LayoutAnimation.easeInEaseOut();
    }

    render() {
        return (
            <Container>
                <Content padder>
                    <View style={{ padding: 10 }}>
                        <Label style={{ fontSize: 15, color: "black" }}>Isi Saldo Ke</Label>
                        <Picker
                            selectedValue="all"
                            style={{ height: 50, width: "100%" }}>
                            <Picker.Item label="Tabungan Waqara Umroh" value="all" />
                        </Picker>

                        <Label style={{ fontSize: 15, color: "black" }}>Dari</Label>
                        <Picker
                            selectedValue="all"
                            style={{ height: 50, width: "100%" }}>
                            <Picker.Item label="ATM / Internet / Mobile Banking" value="all" />
                            <Picker.Item label="ATM" value="atm" />
                            <Picker.Item label="Internet" value="inet" />
                            <Picker.Item label="Mobile Banking" value="mbanking" />
                        </Picker>

                        <Text style={{ color: "#000", fontWeight: "bold", marginBottom: 10 }}>Instruksi</Text>
                        <Text>1. Kunjungi jaringan ATM Bersama / Prima</Text>
                        <Text>2. Pilih menu Transfer</Text>
                        <Text>3. Pilih Bank BNI Syariah atau Masukkan Kode Bank 247</Text>
                        <Text>4. Masukkan No. Tabungan Waqara Umroh Anda</Text>
                        <Text style={{ alignSelf: "center", fontWeight: "bold" }}>123 456 789</Text>
                        <Text>5. Masukkan jumlah saldo yang ingin ditambah</Text>
                        <Text>6. Tunggu notifikasi, Saldo anda akan bertambah dalam waktu 1 x 24 jam.</Text>

                        <Text style={{ marginTop: 20, marginLeft: 10, marginRight: 10, textAlign: 'center' }}>Dengan mengetuk tombol berikut. Anda telah menyetujui Syarat & Ketentuan dan Kebijakan Privasi Waqara.</Text>
                    </View>
                    <Button rounded style={{ marginTop: 20, flex: 1, backgroundColor: "#41854A" }} onPress={ () => this.props.navigation.navigate('TabunganIndex') }>
                        <Text>Selesai</Text>
                    </Button>
                </Content>
            </Container>
        );
    }
}

export default TabunganAdd;
