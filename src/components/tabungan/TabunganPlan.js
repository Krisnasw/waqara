import React, { Component } from 'react';
import {
    View,
    LayoutAnimation,
    Image,
    ImageBackground,
    Picker
} from 'react-native';
import {
    Container,
    Content,
    Text,
    Item,
    Label,
    Input,
    Button
} from 'native-base';

class TabunganPlan extends Component {
    componentWillMount() {
        LayoutAnimation.easeInEaseOut();
    }

    render() {
        return (
            <Container>
                <Content>
                    <ImageBackground
                        style={{ flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'center', height: 200 }}
                        source={require('../../images/bg_perencanaan.png')} />
                    <View style={{ padding: 20 }}>
                        <Text>
                            Rencanakan Tabunganmu dan Wujudkan Perjalanan Umrohmu dengan mudah.
                        </Text>

                        <Text style={{ color: 'grey', fontSize: 14, marginTop: 20 }}>Kapan Perjalanan Umroh Ingin Dilakukan ?</Text>
                        <View style={{ flex:2, flexDirection:"row", justifyContent:'space-between' }}>
                            <View style={{ flex:2 }}>
                                <Picker
                                    selectedValue="all"
                                    style={{ height: 50, width: "100%" }}>
                                    <Picker.Item label="Bulan Keberangkatan" value="all" />
                                </Picker>
                            </View>
                            <View style={{ flex:0.1 }}/>
                            <View style={{ flex:2 }}>
                                <Picker
                                    selectedValue="all"
                                    style={{ height: 50, width: "100%" }}>
                                    <Picker.Item label="Tahun Keberangkatan" value="all" />
                                </Picker>
                            </View>
                        </View>

                        <Item style={{ marginLeft: -1, marginTop: 20 }} stackedLabel>
                            <Label style={{ fontSize: 15, color: "grey" }}>Berapa Jumlah Uang Yang Dipersiapkan</Label>
                            <Input placeholder="Rp " />
                        </Item>
                        <Text style={{ color: '#41854A', fontSize: 12 }}>Estimasi di tahun 20xx biaya umroh min: Rp xxx.xxx.xxx,-</Text>
                    </View>
                </Content>
                <Button full success style={{ backgroundColor: "#41854A" }} onPress={() => this.props.navigation.navigate('TabunganPlanProccess')}>
                    <Text>Lanjut</Text>
                </Button>
            </Container>
        );
    }
}

export default TabunganPlan;
