import React, { Component } from 'react';
import {
    View,
    LayoutAnimation,
    Image,
    TouchableOpacity
} from 'react-native';
import {
    Container,
    Content,
    Text,
    Card,
    CardItem,
    Icon,
    Right
} from 'native-base';

class TabunganCreate extends Component {
    componentWillMount() {
        LayoutAnimation.easeInEaseOut();
    }

    render() {
        return (
            <Container>
                <Content style={{ padding: 20 }}>
                    <View style={{ backgroundColor: "#FFF", alignItems: "center", marginBottom: 20 }}>
                        <Text style={{ color: 'grey', textAlign: 'center' }}>Buat Tabungan Waqara Umroh Dengan Rekan Bank Terpercaya. Mudah. Cepat. Tanpa Perlu Antri</Text>
                    </View>

                    <Card style={{ marginLeft: 10, marginRight: 10, height: 150 }}>
                        <CardItem style={{ height: 50 }} button onPress={ () => this.props.navigation.navigate('UmrohPembiayaan') }>
                            <View style={{ flex: 3, flexDirection: "row" }}>
                                <Image
                                    style={{ width: 50, height: 50 }}
                                    source={require('../../images/logo_bni.png')}
                                    resizeMode="contain"
                                />
                            </View>
                            <Right style={{ flex: 1 }}>
                                <Icon name="chevron-down" type="FontAwesome" style={{ color: "#41854A" }} />
                            </Right>
                        </CardItem>
                        <View style={{ borderBottomColor: "grey", borderBottomWidth: 1 }} />
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('TabunganNew')}>
                            <View style={{ justifyContent: 'center', padding: 12.5 }}>
                                <Text style={{ alignSelf: 'center', color: 'grey' }}>Belum Memiliki Tabungan iB Baitullah</Text>
                            </View>
                        </TouchableOpacity>
                        <View style={{ borderBottomColor: "grey", borderBottomWidth: 1 }} />
                        <TouchableOpacity onPress={ () => this.props.navigation.navigate('TabunganActivation')}>
                            <View style={{ justifyContent: 'center', padding: 12.5 }}>
                                <Text style={{ alignSelf: 'center', color: 'grey' }}>Sudah Memiliki Tabungan iB Baitullah</Text>
                            </View>
                        </TouchableOpacity>
                    </Card>
                </Content>
            </Container>
        );
    }
}

export default TabunganCreate;
