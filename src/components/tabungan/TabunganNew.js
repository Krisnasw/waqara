import React, { Component } from 'react';
import {
    View,
    LayoutAnimation,
    Image,
    Dimensions,
    Picker
} from 'react-native'
import {
    Container,
    Content,
    Text,
    Form,
    Card,
    CardItem,
    Input,
    Button,
    Item,
    Icon,
    Label
} from 'native-base';

class TabunganNew extends Component {
    constructor(props) {
        super(props);
        var date = new Date();
        this.state = {
            language: "all",
            paket: "all",
            bulan: date.getMonth()
        };
    }

    componentWillMount() {
        LayoutAnimation.spring();
    }

    render() {
        const { imageStyles } = styles;
        const { width, height } = Dimensions.get("window");

        return (
            <Container>
                <Content padder>
                    <Card style={{ height: 555 }}>
                        <CardItem>
                            <Content
                                showsVerticalScrollIndicator={false}
                            >
                                <Form style={{ padding: 5 }}>
                                    <Item style={{ marginLeft: -1 }} stackedLabel>
                                        <Label style={{ fontSize: 15, color: "black" }}>Nomor KTP</Label>
                                        <Input keyboardType="numeric" maxLength={20} />
                                    </Item>
                                    
                                    <Item style={{ marginLeft: -1 }} stackedLabel>
                                        <Label style={{ fontSize: 15, color: "black" }}>Tanggal Lahir</Label>
                                        <Input />
                                    </Item>

                                    <Item style={{ marginLeft: -1 }} stackedLabel>
                                        <Label style={{ fontSize: 15, color: "black" }}>Jenis Kelamin</Label>
                                        <Input />
                                    </Item>

                                    <Item style={{ marginLeft: -1 }} stackedLabel>
                                        <Label style={{ fontSize: 15, color: "black" }}>Alamat</Label>
                                        <Input />
                                    </Item>

                                    <Item style={{ marginLeft: -1 }} stackedLabel>
                                        <Label style={{ fontSize: 15, color: "black" }}>Kecamatan</Label>
                                        <Input />
                                    </Item>

                                    <Item style={{ marginLeft: -1 }} stackedLabel>
                                        <Label style={{ fontSize: 15, color: "black" }}>Kota</Label>
                                        <Input />
                                    </Item>

                                    <Item style={{ marginLeft: -1 }} stackedLabel>
                                        <Label style={{ fontSize: 15, color: "black" }}>Provinsi</Label>
                                        <Input />
                                    </Item>
                                </Form>
                            </Content>
                        </CardItem>
                    </Card>
                </Content>
                <Button full style={{ backgroundColor: "#41854A" }} onPress={ () => this.props.navigation.navigate('TabunganNewProccess') } iconRight>
                    <Text>Proses</Text>
                    <Icon type="FontAwesome" name="send" />
                </Button>
            </Container>
        );
    }
}

const styles = {
    imageStyles: {
        width: 150,
        height: 30,
        justifyContent: "center",
        alignSelf: "center"
    }
}

export default TabunganNew;
