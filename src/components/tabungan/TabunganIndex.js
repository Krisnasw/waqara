import React, { Component } from 'react';
import {
    View,
    Image,
    LayoutAnimation
} from 'react-native';
import {
    Container,
    Tab,
    Tabs
} from 'native-base';
import Tab1 from '../tabungan/RincianTab';
import Tab2 from '../tabungan/TransaksiTab';

class TabunganIndex extends Component {
    constructor(props) {
        super(props);
    }

    componentWillMount() {
        LayoutAnimation.easeInEaseOut();
    }

    render() {
        return (
            <Container>
                <Tabs>
                    <Tab heading="Rincian" tabStyle={{backgroundColor: '#FFF'}} textStyle={{color: '#000'}} activeTabStyle={{backgroundColor: '#FFF'}} activeTextStyle={{color: '#41854A', fontWeight: 'normal'}}>
                        <Tab1 navigation={this.props.navigation} />
                    </Tab>
                    <Tab heading="Transaksi" tabStyle={{backgroundColor: '#FFF'}} textStyle={{color: '#000'}} activeTabStyle={{backgroundColor: '#FFF'}} activeTextStyle={{color: '#41854A', fontWeight: 'normal'}}>
                        <Tab2 navigation={this.props.navigation} />
                    </Tab>
                </Tabs>
            </Container>
        );
    }
}

export default TabunganIndex;
