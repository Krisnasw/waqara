import React, { Component } from 'react';
import {
    View,
    LayoutAnimation,
    Image,
    ImageBackground,
    Picker
} from 'react-native';
import {
    Container,
    Content,
    Text,
    Item,
    Label,
    Input,
    Button
} from 'native-base';

class TabunganPlanProccess extends Component {
    componentWillMount() {
        LayoutAnimation.easeInEaseOut();
    }

    render() {
        return (
            <Container>
                <Content>
                    <ImageBackground
                        style={{ flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'center', height: 200 }}
                        source={require('../../images/bg_perencanaan.png')} />
                    <View style={{ padding: 20 }}>
                        <View style={{ flexDirection: "column" }}>
                            <Text>Perjalanan Umroh : {"\t"} Juni 2033</Text>
                            <Text>Estimasi Biaya : {"\t"} Rp 30.000.000,-</Text>
                        </View>

                        <Item style={{ marginLeft: -1, marginTop: 20 }} stackedLabel>
                            <Label style={{ fontSize: 15, color: "grey" }}>Saldo Awal Tabungan Waqara Umroh</Label>
                            <Input placeholder="Rp 1.000.000" />
                        </Item>

                        <Text style={{ color: 'grey', fontSize: 14, marginTop: 20 }}>Tabungan Rutin ?</Text>
                        <View style={{ flex:2, flexDirection:"row", justifyContent:'space-between' }}>
                            <View style={{ flex:2 }}>
                                <Picker
                                    selectedValue="all"
                                    style={{ height: 50, width: "100%" }}>
                                    <Picker.Item label="Bulanan" value="all" />
                                </Picker>
                            </View>
                            <View style={{ flex:0.1 }}/>
                            <View style={{ flex:2 }}>
                                <Picker
                                    selectedValue="all"
                                    style={{ height: 50, width: "100%" }}>
                                    <Picker.Item label="Rp 500.000" value="all" />
                                </Picker>
                            </View>
                        </View>
                    </View>
                </Content>
                <Button full success style={{ backgroundColor: "#41854A" }} onPress={() => this.props.navigation.navigate('TabunganIndex')}>
                    <Text>Lanjut</Text>
                </Button>
            </Container>
        );
    }
}

export default TabunganPlanProccess;
