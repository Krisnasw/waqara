import React, { Component } from 'react';
import {
    View,
    LayoutAnimation,
    Image,
    Dimensions,
    TouchableOpacity
} from 'react-native';
import {
    Container,
    Content,
    Text,
    Card,
    CardItem,
    Button,
    Grid,
    Col
} from 'native-base';

class RincianTab extends Component {
    constructor(props) {
        super(props);
    }

    componentWillMount() {
        LayoutAnimation.easeInEaseOut();
    }

    render() {
        const { width, height } = Dimensions.get("window");

        return (
            <Container>
                <Content>
                    <View style={{ backgroundColor: "#FFF", alignItems: "center", marginBottom: 20 }}>
                        <Image
                            style={{ width: 100, height: 100, alignSelf: "center" }}
                            source={require('../../images/logo_bni.png')}
                            resizeMode="contain"
                        />
                        <Text style={{ color: 'grey' }}>Tipe Rekening : ib Baitullah</Text>
                        <Text style={{ color: 'grey' }}>No Rekening 123456789</Text>
                        <Text style={{ color: 'grey' }}>A/N.Abdullah Wijaya</Text>
                    </View>

                    <Card style={{ backgroundColor: '#F2F2F2', width: width - 110, alignSelf: 'center' }}>
                        <CardItem style={{ flexDirection: "column", backgroundColor: "#F2F2F2" }}>
                            <Text style={{ fontSize: 10 }}>Saldo Tabungan Waqara Umroh</Text>
                            <Text style={{ color: "#41854A", fontWeight: "bold" }}>Rp 1.000.000</Text>
                        </CardItem>
                    </Card>

                    <View style={{ marginTop: 10 }} />

                    <Button light style={{ borderColor: '#41854A', backgroundColor: 'white', borderWidth: 1, alignSelf: 'center' }} onPress={() => this.props.navigation.navigate('TabunganPlan')}>
                        <Text style={{ color: "#41854A" }}>Buat Perencanaan Tabungan</Text>
                    </Button>
                    
                    <Grid style={{ marginTop: 35, justifyContent: 'center' }}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('TabunganAdd')}>
                            <Col style={{ borderRadius: 10, borderWidth: 1, borderColor: "#41854A", height: 75, marginLeft: 10, marginRight: 10, width: 80 }}>
                                <View style={{ alignItems: "center", justifyContent: "center", flex: 1 }}>
                                    <Image 
                                        style={{ width: 24, height: 24, margin: 5 }}
                                        source={require('../../images/tambah.png')}
                                        resizeMode="contain"
                                    />
                                    <Text style={{ color: '#000', fontSize: 12 }}>Tambah</Text>
                                </View>
                            </Col>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('TabunganWd')}>
                            <Col style={{ borderRadius: 10, borderWidth: 1, borderColor: "#41854A", height: 75, marginLeft: 10, marginRight: 10, width: 80 }}>
                                <View style={{ alignItems: "center", justifyContent: "center", flex: 1 }}>
                                    <Image 
                                        style={{ width: 24, height: 24, margin: 5 }}
                                        source={require('../../images/tarik.png')}
                                        resizeMode="contain"
                                    />
                                    <Text style={{ color: '#000', fontSize: 12 }}>Tarik</Text>
                                </View>
                            </Col>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('UmrohForm')}>
                            <Col style={{ borderRadius: 10, borderWidth: 1, borderColor: "#41854A", height: 75, marginLeft: 10, marginRight: 10, width: 80 }}>
                                <View style={{ alignItems: "center", justifyContent: "center", flex: 1 }}>
                                    <Image
                                        style={{ width: 24, height: 24, margin: 5 }}
                                        source={require('../../images/cari_paket.png')}
                                        resizeMode="contain"
                                    />
                                    <Text style={{ color: '#000', fontSize: 12 }}>Cari Paket</Text>
                                </View>
                            </Col>
                        </TouchableOpacity>
                    </Grid>
                </Content>
            </Container>
        );
    }
}

export default RincianTab;
