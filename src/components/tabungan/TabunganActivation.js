import React, { Component } from 'react';
import {
    View,
    LayoutAnimation,
    Image,
    Dimensions,
    Keyboard
} from 'react-native';
import {
    Container,
    Content,
    Text,
    Form,
    Item,
    Label,
    Input,
    Button
} from 'native-base';

class TabunganActivation extends Component {
    componentWillMount() {
        LayoutAnimation.easeInEaseOut();
    }

    componentWillUnmount() {
        Keyboard.dismiss();
    }

    render() {
        const { width, height } = Dimensions.get("window");

        return (
            <Container>
                <Content style={{ padding: 20 }}>
                    <View style={{ backgroundColor: "#FFF", alignItems: "center", marginBottom: 10 }}>
                        <Image
                            style={{ width: 100, height: 100, alignSelf: "center" }}
                            source={require('../../images/logo_bni.png')}
                            resizeMode="contain"
                        />
                    </View>

                    <Form style={{ padding: 5 }}>
                        <Item style={{ marginLeft: -1 }} stackedLabel>
                            <Label style={{ fontSize: 15, color: "black" }}>Nomor Rekening iB Baitullah</Label>
                            <Input keyboardType="numeric" maxLength={20} />
                        </Item>
                        
                        <Item style={{ marginLeft: -1 }} stackedLabel>
                            <Label style={{ fontSize: 15, color: "black" }}>Pin</Label>
                            <Input keyboardType="numeric" secureTextEntry />
                        </Item>
                    </Form>

                    <Button success rounded style={{ marginTop: 20, alignSelf: 'center', borderWidth: 1, borderColor: "#FFF", shadowOpacity: 0, backgroundColor: "#41854A" }} onPress={ () => this.props.navigation.navigate('TabunganIndex') }>
                        <Text style={{ color: '#FFF' }}>Verifikasi Tabungan</Text>
                    </Button>
                </Content>
            </Container>
        );
    }
}

export default TabunganActivation;
