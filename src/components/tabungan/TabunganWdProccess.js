import React, { Component } from 'react';
import {
    View,
    LayoutAnimation,
    Image,
    Dimensions
} from 'react-native';
import {
    Container,
    Content,
    Text,
    Card,
    CardItem,
    Button
} from 'native-base';

class TabunganWdProccess extends Component {
    componentWillMount() {
        LayoutAnimation.easeInEaseOut();
    }

    render() {
        const { width, height } = Dimensions.get("window");

        return (
            <Container>
                <Content style={{ padding: 20 }}>
                    <View style={{ backgroundColor: "#FFF", alignItems: "center", marginBottom: 20 }}>
                        <Image
                            style={{ width: 100, height: 100, alignSelf: "center" }}
                            source={require('../../images/logo_bni.png')}
                            resizeMode="contain"
                        />
                        <Text style={{ color: 'grey', textAlign: 'center' }}>Anda telah melakukan penarikan dana tabungan waqara umroh anda</Text>

                        <Text style={{ color: 'black', textAlign: 'center', fontWeight: "bold", marginTop: 20, fontSize: 15 }}>Tunjukkan Kode Aktivasi Anda Sebagai berikut di Cabang BNI Syariah terdekat</Text>
                    </View>

                    <Card style={{ backgroundColor: '#F2F2F2', width: width - 110, alignSelf: 'center' }}>
                        <CardItem style={{ flexDirection: "column", backgroundColor: "#F2F2F2" }}>
                            <Text style={{ color: "#41854A", fontWeight: "bold", fontSize: 30 }}>TAR3928450IK</Text>
                        </CardItem>
                    </Card>

                    <Text style={{ color: 'grey', textAlign: 'center', marginTop: 20 }}>Kode ini berlaku mulai 1x24 jam sejak pengajuan sampai dengan 14 hari kerja. Setelah melewati periode tersebut kode tidak laku.</Text>

                    <Button light rounded style={{ marginTop: 20, alignSelf: 'center', borderWidth: 1, borderColor: "#41854A", shadowOpacity: 0 }} onPress={ () => this.props.navigation.navigate('Home') }>
                        <Text style={{ color: '#41854A' }}>Kembali Ke Menu Utama</Text>
                    </Button>
                </Content>
            </Container>
        );
    }
}

export default TabunganWdProccess;
