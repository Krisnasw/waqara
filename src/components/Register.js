import React, { Component } from 'react';
import {
    Container,
    Content,
    StyleProvider,
    Header,
    Title,
    Form,
    Icon,
    Input,
    Button,
    Item,
    Label,
    Text
} from 'native-base';
import {
    LayoutAnimation
} from 'react-native';
import getTheme from '../../native-base-theme/components';
import material from '../../native-base-theme/variables/material';

class Register extends Component {
    componentWillMount() {
        LayoutAnimation.easeInEaseOut();
    }

    onButtonPress() {
        this.props.navigation.navigate('Login');
    }

    render() {
        const { headerText, contentStyle, buttonStyles, buttonTextStyles, dividerStyles } = styles;

        return (
            <StyleProvider style={getTheme(material)}>
                <Container style={{ flex: 1 }}>
                    <Header style={{ backgroundColor: "#41854A" }}>
                        <Title style={headerText}>Daftar Member Waqara</Title>
                    </Header>
                    <Content>
                        <Form>
                            <Content style={{ padding: "5%" }}>
                                <Item stackedLabel>
                                    <Label>Nama Lengkap</Label>
                                    <Input 
                                        placeholder="e.g Budi Setiawan"
                                    />
                                </Item>
                                <Item stackedLabel>
                                    <Label>Email</Label>
                                    <Input
                                        keyboardType="email-address"
                                        autoCorrect={false}
                                        placeholder="e.g yourmail@mail.com"
                                    />
                                </Item>
                                <Item stackedLabel>
                                    <Label>No.Handphone</Label>
                                    <Input
                                        autoCorrect={false}
                                        placeholder="e.g +6281111111"
                                    />
                                </Item>
                                <Item stackedLabel>
                                    <Label>Password</Label>
                                    <Input
                                        secureTextEntry
                                    />
                                </Item>

                                <Button rounded light style={buttonStyles}>
                                    <Text style={buttonTextStyles}>Daftar Sekarang</Text>
                                </Button>

                                <Text style={dividerStyles}>
                                ───────  Atau  ───────
                                </Text>

                                <Button iconLeft rounded primary style={buttonStyles} onPress={() => this.onButtonPress()}>
                                    <Icon type="FontAwesome" android="user" ios="user" style={{ paddingTop: 15 }} />
                                    <Text style={{ color: "#FFF", paddingTop: 15 }}>Sudah Punya Akun ? Login</Text>
                                </Button>
                            </Content>
                        </Form>
                    </Content>
                </Container>
            </StyleProvider>
        );
    }
};

const styles = {
    headerText: {
        justifyContent: "center",
        alignSelf: "center"
    },
    contentStyle: {
        marginLeft: 10,
        marginRight: "5%"
    },
    buttonStyles: {
        margin: "5%",
        marginLeft: 0,
        marginRight: 0,
        flex: 1,
        justifyContent: "center",
        alignSelf: "center"
    },
    buttonTextStyles: {
        color: "#41854A",
        paddingTop: 15
    },
    dividerStyles: {
        justifyContent: "center",
        alignSelf: "center",
        color: "#000"
    }
}

export default Register;
