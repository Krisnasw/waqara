import React, { Component } from "react";
import {
	Text,
	Keyboard
} from "react-native";
import {
	Container,
	Content,
	StyleProvider,
	Button,
	Footer,
	FooterTab,
	Icon
} from 'native-base';
import getTheme from '../../native-base-theme/components';
import material from '../../native-base-theme/variables/material';
import Profile from './common/Profile';
import Main from './common/Main';
import Help from './common/Help';
import Notif from './common/Notif';
import Dashboard from './common/Dashboard';

class Home extends Component {
	constructor(props) {
		super(props);
		this.state = { index: 0 }
	}

	componentWillMount() {
		Keyboard.dismiss();
	}

	switchScreen(index) {
		this.setState({ index: index })
	}

	render() {
		const { imageStyles, container, textStyles, walletStyle, textTabunganStyle, textWalletStyle, bottomBarStyle, iconTabStyle } = styles;

		let AppComponent = null;

		switch (this.state.index) {
			case 0:
				AppComponent = Main;
				break;
			case 1:
				AppComponent = Dashboard;
				break;
			case 2:
				AppComponent = Help;
				break;
			case 3:
				AppComponent = Notif;
				break;
			case 4:
				break;
			case 5:
				AppComponent = Profile;
				break;
			default:
				AppComponent = Main;
				break;
		}

		return (
			<StyleProvider style={getTheme(material)}>
				<Container style={{ flex: 1 }}>
					<Content style={container}>
						<AppComponent navigation={this.props.navigation} />
					</Content>
					<Footer>
						<FooterTab style={bottomBarStyle}>
							<Button
								vertical
								active={this.state.index === 0}
								onPress={() => this.switchScreen(0)}>
								<Icon name="home" />
								<Text>Beranda</Text>
							</Button>
							<Button
								vertical
								active={this.state.index === 1}
								onPress={() => this.switchScreen(1)}>
								<Icon type="FontAwesome" name="align-justify" />
								<Text>Dashboard</Text>
							</Button>
							<Button 
								vertical
								active={this.state.index === 2}
								onPress={() => this.switchScreen(2)}>
								<Icon active type="FontAwesome" android="question-circle" ios="question-circle" />
								<Text>Bantuan</Text>
							</Button>
							<Button
								vertical
								active={this.state.index === 3}
								onPress={() => this.switchScreen(3)}>
								<Icon name="mail" />
								<Text>Pesan</Text>
							</Button>
							<Button
								vertical
								active={this.state.index === 5}
								onPress={() => this.switchScreen(5)}>
								<Icon type="FontAwesome" name="user" />
								<Text>Akun</Text>
							</Button>
						</FooterTab>
					</Footer>
				</Container>
			</StyleProvider>
		);
	}
}

const styles = {
	imageStyles: {
		width: 150,
		height: 30,
		justifyContent: "center",
		alignSelf: "center"
	},
	container: {
		flex: 1,
		backgroundColor: "#F9F9F9"
	},
	textStyles: {
		color: "#FFFFFF",
		fontSize: 20
	},
	walletStyle: {
		height: 100,
		backgroundColor: "#41854A"
	},
	textTabunganStyle: {
		fontSize: 15,
		marginLeft: 20,
		marginTop: 20,
		color: "#FFFFFF"
	},
	textWalletStyle: {
		fontSize: 15,
		marginLeft: 20,
		color: "#FFFFFF"
	},
	bottomBarStyle: {
		backgroundColor: "#FFF"
	}
}

export default Home;
